function [shift, cc] = calc_normxcorr_matlab(RF0,RF1,kernel_samples,search_samples,downsample_factor)
for i = 1:ndims(RF1)
if size(RF0,i) == 1;
    rep = ones(1,ndims(RF1));rep(i) = size(RF1,i);
    RF0 = repmat(RF0,rep);
end
end
search_idx = floor(-search_samples/2):ceil(search_samples/2);
kernel_idx = floor(-kernel_samples/2):ceil(kernel_samples/2);
N = size(RF1,1);
out_idx = (1:downsample_factor:N);
out_idx = round(out_idx(:)+floor((N-out_idx(end))/2));
[out_Idx, search_Idx] = meshgrid(out_idx,search_idx);
calc_idx = unique(out_Idx(:)+search_Idx(:));
calc_idx = calc_idx(calc_idx>0 & calc_idx<=N);
sz = size(RF1);
[shift,cc] = deal(zeros([length(out_idx),sz(2:end)],'single'));
N = prod(sz(2:end));
parfor idx = 1:N
    ncc = calc_ncc(RF0(:,idx),RF1(:,idx),kernel_idx,search_idx,out_idx,calc_idx);
    [cc(:,idx), shift(:,idx)] = subsamplepeak(search_idx,ncc,2);
end
end

function ncc = calc_ncc(ref,tgt,kernel_idx,search_idx,out_idx,calc_idx)
kl = length(kernel_idx);
sl = length(search_idx);
Nout = length(out_idx);
N = size(tgt,1);
[k_tgt, k_ref] = deal(nan(N,length(kernel_idx)));
for i = 1:length(kernel_idx);
    k_tgt(calc_idx,i) = tgt(max(1,min(N,calc_idx+kernel_idx(i))));
    k_tgt(calc_idx(calc_idx+kernel_idx(i)<1 | calc_idx+kernel_idx(i)>N),i) = nan;
    k_ref(calc_idx,i) = ref(max(1,min(N,calc_idx+kernel_idx(i))));
    k_ref(calc_idx(calc_idx+kernel_idx(i)<1 | calc_idx+kernel_idx(i)>N),i) = nan;
end
k_tgt = (k_tgt - repmat(nanmean(k_tgt,2),[1 kl]))./repmat(nanstd(k_tgt,0,2),[1 kl]);
k_ref = (k_ref - repmat(nanmean(k_ref,2),[1 kl]))./repmat(nanstd(k_ref,0,2),[1 kl]);
ncc = nan(Nout,sl);
for i = 1:sl;
    zidxtgt = max(1,min(N,out_idx+search_idx(i)));
    zidxref = max(1,min(N,out_idx-search_idx(i)));
    ncc(:,i) = (1/kl)*(nansum(k_ref(zidxref,:).*k_tgt(zidxtgt,:),2));
end
end
