function d = calc_spline_matlab(s1,s2,f_samp);
% s1: window signal
% s2: kernel signal
% f samp: sampling frequency
x=[0:length(s1)-1];
y=spline(x,s1); % generate cubic spline coefficients
len=length(s1)-length(s2);
for i=1:len % slide kernel across window
c=y.coefs(i:i+length(s2)-1,:);
[r(i),v(i)]=msse(c,s2);
end
p=find(r>=0&r<1);
if isempty(p)
    %d = NaN;
    %return
    p = find(abs(r)==min(abs(r)));
end
[m,j]=min(abs(v(p)));
d=(-round((len+1)/2)+p(j)+r(p(j)))*1/f_samp;

% estimated time delay
function [x,v]=msse(c,y);
[n,m]=size(c); p=[c(:,1)'*c(:,1) 2*(c(:,1)'*c(:,2))...
2*(c(:,1)'*c(:,3))+c(:,2)'*c(:,2)...
2*(c(:,1)'*c(:,4))+2*(c(:,2)'*c(:,3))-2*(c(:,1)'*y)...
2*(c(:,2)'*c(:,4))+c(:,3)'*c(:,3)-2*(c(:,2)'*y)...
2*(c(:,3)'*c(:,4))-2*(c(:,3)'*y)...
c(:,4)'*c(:,4)+y'*y-2*(c(:,4)'*y)]; % implement (7)
r=roots([6*p(1) 5*p(2) 4*p(3) 3*p(4) 2*p(5) p(6)]);
nr=r(find(imag(r)==0));
for i=1:length(nr)
for j=1:7
x_matrix(j,i)=nr(i)^(7-j);
end
end
values=p*x_matrix;
[v,i]=min(values);
x=nr(i); % local delay estimate