% phaseShiftRadians = CALC_KASAI(IQdataRef,IQdataTgt,kLength_samples,decimation_factor)
%
% CALC_KASAI return the phase shift (displacement) using autocorrelation
% between two IQ signals IQdataRef and IQdataTgt.
%
% INPUTS:
%   IQdataRef - complex reference IQ data with the first dimension being
%       fast time (range). If the size of IQdataRef is smaller than 
%       IQdataTgt, IQdataRef will be expanded with repmat to match the 
%       size (i.e.) for a single, anchored reference frame.
%   IQdataRef - complex target IQ data with the first dimension being fast 
%       time (range)
%   kLength_samples - the axial smoothing kernel length in samples. This
%       can be calculated as round(k(wavelengths)*fs/f0).
%   [decimation_factor] - optional parameter to only report the phase
%       shifts at a downsampled spacing. Defaults to 1
%
% OUTPUTS:
%   phaseShiftRadians - The computed phase shift in RADIANS, the same size
%       as IQdataTgt. To compute the displacements, multiply by 
%       (c/2)/f0 * (1/(2*pi))

% Revision History:
% Created 5/14/2015 pjh7

function phaseShiftRadians = calc_kasai(IQdataRef,IQdataTgt,kLength_samples,decimation_factor)
for i = 1:ndims(IQdataTgt)
    if size(IQdataRef,i) == 1;
        rep = ones(1,ndims(IQdataTgt));
        rep(i) = size(IQdataTgt,i);
        IQdataRef = repmat(IQdataRef,rep);
    end
end
if ~exist('decimation_factor','var') || isempty(decimation_factor)
    decimation_factor = 1;
end
nu01 = imag(IQdataTgt).*real(IQdataRef)-real(IQdataTgt).*imag(IQdataRef);
de01 = real(IQdataTgt).*real(IQdataRef)+imag(IQdataTgt).*imag(IQdataRef);    
if decimation_factor == 1;
    K = ones(kLength_samples,1)./kLength_samples;
    Nu01 = convn(nu01,K,'same');
    De01 = convn(de01,K,'same');
    phaseShiftRadians  = -1*atan2(Nu01,De01);
else
    sz = size(IQdataTgt);
    idx = 1:decimation_factor:sz;
    [Nu01,De01] = deal(zeros([kLength_samples length(idx) sz(2:end)],'single'));
    offsets = (1:kLength_samples)-round(kLength_samples/2);
    for i = 1:kLength_samples
        idx1 = max(1,min(sz(1),idx+offsets(i)));
        Nu01(i,:) = reshape(nu01(idx1,:),[],1);
        De01(i,:) = reshape(de01(idx1,:),[],1);
    end
    phaseShiftRadians = -1*reshape(atan2(sum(Nu01,1)/kLength_samples,sum(De01,1)/kLength_samples),[length(idx),sz(2:end)]);
end
end