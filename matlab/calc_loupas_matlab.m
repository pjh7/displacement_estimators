function [phi] = calc_loupas_matlab(refi,tgti,kLength_samp,fdemOverfs,tgt_autocorr_flag)
%CALC_LOUPAS unitless loupas estimator
%   Detailed explanation goes here
if ~exist('tgt_autocorr_flag','var')
    tgt_autocorr_flag = 1;
end

phi = zeros(size(tgti),'single');

refq = imag(refi(:));
refi = real(refi(:));
tgtq = imag(tgti(:));
tgti = real(tgti(:));

refq = repmat(refq,[size(tgti,1)/size(refi,1),1]);
refi = repmat(refi,[size(tgti,1)/size(refi,1),1]);

QI=refq.*tgti;
IQ=refi.*tgtq;
II=refi.*tgti;
QQ=refq.*tgtq;

kernel = [0;ones(kLength_samp,1)]/kLength_samp;
n1=convn(QI,kernel,'same');
n2=convn(IQ,kernel,'same');
n3=convn(II,kernel,'same');
n4=convn(QQ,kernel,'same');

refip1 = circshift(refi,-1,1);
refqp1= circshift(refq,-1,1);

if tgt_autocorr_flag
    tgtip1 = circshift(tgti,-1,1);
    tgtqp1= circshift(tgtq,-1,1);
    QI2=refq.*refip1 + tgtq.*tgtip1;
    IQ2=refi.*refqp1 + tgti.*tgtqp1;
    II2=refi.*refip1 + tgti.*tgtip1;
    QQ2=refq.*refqp1 + tgtq.*tgtqp1;
else
    QI2=refq.*refip1;
    IQ2=refi.*refqp1;
    II2=refi.*refip1;
    QQ2=refq.*refqp1;
end

kernel = [0; 0; ones(kLength_samp-1,1)]/(kLength_samp-1);
d1=convn(QI2,kernel,'same');
d2=convn(IQ2,kernel,'same');
d3=convn(II2,kernel,'same');
d4=convn(QQ2,kernel,'same');

if numel(fdemOverfs)==1
phi(:) = (fdemOverfs*atan2(n1-n2,n3+n4))./(fdemOverfs+atan2(d2-d1,d3+d4)/(2*pi));
else
    fdemOverfs = repmat(fdemOverfs,[size(tgti,1)/length(fdemOverfs) 1]);
phi(:) = (fdemOverfs.*atan2(n1-n2,n3+n4))./(fdemOverfs+atan2(d2-d1,d3+d4)/(2*pi));
end

% i = 1:numel(n1);
% p = plot(i,n1-n2,'bo',i,d2-d1,'ro',i,n3+n4,'mo',i,d3+d4,'ko');%,i,(d1-d2)./(d3+d4),'co');
% set(p,'MarkerSize',4,'linewidth',3)
% set(p(1),'DisplayName','Mex N1');
% set(p(2),'DisplayName','Mex N2');
% set(p(3),'DisplayName','Mex D1');
% set(p(4),'DisplayName','Mex D2');
% %set(p(5),'DisplayName','Mex N2/N2');


end

