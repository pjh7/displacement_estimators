%function [shift, cc, corr] = calc_iqnormxcorr_matlab(iq_ref,iq_tgt,fs,f0,fdem,ksz_lambda,srch_max_lambda,srch_inc_lambda)
function [shift, cc, corr] = calc_iqnormxcorr_matlab(iq_ref,iq_tgt,fdemoverfs,ksz_samp,srch_max_samp,srch_inc_samp,downsamplefactor)
%period = 1/f0;
%ksz_t = ksz_lambda*period;
%srch_max_t = srch_max_lambda*period;
%srch_inc_t = srch_inc_lambda*period;
%ksz_samp = round(ksz_t*fs);
if ~exist('downsamplefactor','var')
    downsamplefactor = 1;
end
if ~mod(ksz_samp,2)
    ksz_samp = ksz_samp+1;
    warning('ksz:even','Kernel size must be odd. Using %g samples',ksz_samp);
end
%srch_max_samp = ceil(srch_max_t*fs);
srch_margin = round(srch_max_samp);
srch_samp = srch_margin*2 + ksz_samp;
tau_samp = [-srch_max_samp:srch_inc_samp:srch_max_samp];
%tau = [-srch_max_t:srch_inc_t:srch_max_t];
%tau_samp = tau*fs;
k = 1/2*linspace(-1,1,srch_samp)';
%k = k_Hz/fs;
if numel(fdemoverfs)>1
    dyn_fdem = 1;
else
    dyn_fdem = 0;
    G = exp(-1i*2*pi*(k+fdemoverfs)*tau_samp);
    g = fftshift(ifft(ifftshift(G,1),[],1),1);
end
nref = numel(iq_ref);
ntgt = numel(iq_tgt);
ntau = numel(tau_samp);
ref_offset = [-floor(ksz_samp/2):floor(ksz_samp/2)]';
tgt_offset = [-(floor(ksz_samp/2)+ceil(srch_max_samp)):(floor(ksz_samp/2)+ceil(srch_max_samp))]';
outsz = size(iq_tgt);
nlines = prod(outsz(2:end));
outsz(1) = floor((outsz(1)-1)/downsamplefactor)+1;
numel_aline = size(iq_tgt,1);
numel_aline_out = outsz(1);
cc = zeros(outsz);
shift = zeros(outsz);
if nargout>2
corr = zeros([ntau outsz]);
end
for i = 1:numel_aline_out;        
    ii = (downsamplefactor*(i-1));
    if dyn_fdem
        G = exp(-1i*2*pi*(k+fdemoverfs(ii+1))*tau_samp);
        g = fftshift(ifft(ifftshift(G,1),[],1),1); 
    end
    for j = 1:nlines;
        outidx = (j-1)*numel_aline_out + i;
        refidx =  ((j-1)*numel_aline)+mod(ii+ref_offset,numel_aline)+1;
        srchidx = ((j-1)*numel_aline)+mod(ii+tgt_offset,numel_aline)+1;
        iq_ref1 = iq_ref(refidx);
        iq_ref1 = (iq_ref1 - mean(iq_ref1))./std(iq_ref1,1);
        iq_srch = convn(g,iq_tgt(srchidx),'same');
        iq_srch = iq_srch((srch_margin+1):(srch_samp-srch_margin),:);
        iq_srch = (iq_srch - repmat(mean(iq_srch),ksz_samp,1))./repmat(std(iq_srch,1),ksz_samp,1);
        corrtmp = mean(conj(repmat(iq_ref1,1,ntau)).*iq_srch)';
        [cc(outidx), shift(outidx)] = subsamplepeak(tau_samp,abs(corrtmp));
        if nargout>3
            corr(:,outidx) = corrtmp;
        end
    end
end