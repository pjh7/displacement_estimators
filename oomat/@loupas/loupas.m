classdef loupas < displacementestimator
    properties
        %options = struct('kernel_lambda',1.5,'fs_out',[]);
        tgt_autocorr = 1;
        fs_out = [];
    end
    properties (Hidden = true,SetAccess = private);
        inputType = 'iqdata';
    end
     methods (Hidden)
        function estobj = loupas(varargin)
            estobj = setprops(estobj,varargin);
        end
        function  [arfiobj,ccobj] = estimatedisp(estobj,iqobj0,iqobj1)
            params = iqobj1.params;
            kernel_lambda = estobj.kernel_lambda;
            kLength_samp = round(kernel_lambda*params.fs/params.f0);
            if kLength_samp<3
                warning('kernel:tooSmall','%g wavelength kernel is only %g samples. Results may be poor.',kernel_lambda,kLength_samp);
            end
            if isempty(estobj.fs_out)
                estobj.fs_out = params.fs;
            end
            if estobj.fs_out<params.fs;
                if mod(params.fs/estobj.fs_out,1)
                    fs_out = params.fs/floor(params.fs/estobj.fs_out);
                    %warning('fs:ratio','fs/fs_out is not an integer. Using fs_out = %g Hz instead.',estobj.fs_out);
                else
                    fs_out = estobj.fs_out;
                end
                downsample_factor = params.fs/fs_out;
            else
                downsample_factor = 1;
                fs_out = params.fs;
            end
            
            if isfield(iqobj0.params,'fdem')
                fdemOverfs = iqobj0.params.fdem/iqobj0.params.fs;
            else
                fdemOverfs = -iqobj0.params.f0/iqobj0.params.fs;
            end
            
            t0 = tic;
            phi = calc_loupas(iqobj0.data,iqobj1.data,kLength_samp,abs(fdemOverfs),downsample_factor,estobj.tgt_autocorr);
            t1 = toc(t0);
            
            arfiobj = datacontainer(iqobj1);
            arfiobj.data = phi;
            arfiobj = arfiobj*(1e6*(params.c/2)./(-1*fdemOverfs*iqobj0.params.fs)*(1/(2*pi)));
            arfiobj.units = sprintf('\265m');
            arfiobj.log = char(iqobj1.log,sprintf('Loupas'' Algorithm, %g lambda (%g sample) kernel, (%0.3fs)',estobj.kernel_lambda,kLength_samp,t1));
            arfiobj.params.disp_est = estobj;
            if downsample_factor>1
                arfiobj.params.fs = fs_out;
                arfiobj.dims.(arfiobj.dims.names{1}) = arfiobj.dims.(arfiobj.dims.names{1})(((0:size(phi,1)-1)*downsample_factor)+1);
            end
            if arfiobj.params.fs ~= estobj.fs_out
                arfiobj = resample(arfiobj,estobj.fs_out);
            end
            if nargout>1
                ccobj = arfiobj;
                data0 = abs(iqobj0.data(:,:));
                data1 = abs(iqobj1.data(:,:));
                for j = 1:size(data0,2)
                ccobj.data(:,j) = calc_normxcorrcoef(data0(:,j),data1(:,j),kLength_samp,downsample_factor);
                end
                ccobj.units = 'cc\_coef';
            end
        end
        function msg = log(estobj)
            msg = sprintf('Loupas''s Algorithm, k = %0.2g wavelengths',estobj.kernel_lambda);
        end
     end
end