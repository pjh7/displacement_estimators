function outVector = solveMultiRes(dataMatrix,multiResMatrix,borderSamples);
isLimited = nargin>2;
if ~isLimited
    borderSamples = inf;
end
hasTransform = nargin>1;
nSamples = size(dataMatrix,1);
if ~hasTransform
    multiResMatrix = calcMultiResMatrix(nSamples,borderSamples);
end
if size(dataMatrix,2) == 1
    dataVector = dataMatrix;
else
ii = 1;
for i = 1:nSamples-1;
    for j = i+1:min(nSamples,i+1+borderSamples*2);
        dataVector(ii,1) = dataMatrix(i,j);
        %dataMatrix(i,j) = dataVector(ii);
        ii = ii+1;
    end
end
end
outVector = multiResMatrix*dataVector;