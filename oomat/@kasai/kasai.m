classdef kasai < displacementestimator
    properties
        fs_out = [];
    end
    properties (Hidden = true,SetAccess = private);
        inputType = 'iqdata';
    end
    methods (Hidden)
        function obj = kasai(varargin)
            obj = setprops(obj,varargin);
        end
        function  [arfiobj,ccobj] = estimatedisp(estobj,iqobj0,iqobj1)
            params = iqobj1.params;
            kernel_lambda = estobj.kernel_lambda;
            kLength_samp = round(kernel_lambda*params.fs/params.f0);
            if kLength_samp<3
                warning('kernel:tooSmall','%g wavelength kernel is only %g samples. Results may be poor.',kernel_lambda,kLength_samp);
            end
            if isempty(estobj.fs_out)
                estobj.fs_out = params.fs;
            end
            if estobj.fs_out<params.fs;
                if mod(params.fs/estobj.fs_out,1)
                    fs_out = params.fs/floor(params.fs/estobj.fs_out);
                    %warning('fs:ratio','fs/fs_out is not an integer. Using fs_out = %g Hz instead.',estobj.fs_out);
                else
                    fs_out = estobj.fs_out;
                end
                downsample_factor = params.fs/fs_out;
            else
                downsample_factor = 1;
            end
            t0 = tic;
            u = (params.c/2)/params.f0 * (1/(2*pi)) * calc_kasai(iqobj0.data,iqobj1.data,kLength_samp,downsample_factor);
            t1 = toc(t0);
            arfiobj = datacontainer(iqobj1);
            arfiobj.log = char(arfiobj.log,sprintf('Kasai''s Algorithm, k = %0.2g wavelengths (%g samples) (%0.3fs)',estobj.kernel_lambda,kLength_samp,t1));
            arfiobj.data = 1e6*u;
            arfiobj.units = sprintf('\265m');
            arfiobj.params.disp_est = estobj;
            
            if downsample_factor>1
                arfiobj.params.fs = fs_out;
                arfiobj.dims.(arfiobj.dims.names{1}) = arfiobj.dims.(arfiobj.dims.names{1})(1:downsample_factor:end);
            end
            if arfiobj.params.fs ~= estobj.fs_out
                arfiobj = resample(arfiobj,estobj.fs_out);
            end
            if nargout>1
                ccobj = arfiobj;
                data0 = abs(iqobj0.data(:,:));
                data1 = abs(iqobj1.data(:,:));
                ccobj.data(:,:) = calc_normxcorrcoef(data0,data1,kLength_samp,downsample_factor);
                ccobj.units = 'cc\_coef';
            end
        end
    end
end