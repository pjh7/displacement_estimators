function multiResMatrix = calcMultiResMatrix(nSamples,borderSamples,tikhonovScaling,weightMatrix,allowJointatIndex)
isLimited = nargin>1;
hasJoints = nargin>4;
isWeighted = nargin>3;
isRegularized = nargin>2;
if ~isLimited
    borderSamples = inf;
end
if isinf(borderSamples)
    borderSamples = nSamples-1;
end
if ~isRegularized
    tikhonovScaling = 0;
end
ii = 1;
if isWeighted && ~isempty(weightMatrix)
    if size(weightMatrix,2) == 1
        w = weightMatrix;
    elseif size(weightMatrix,1) == 1
        w = weightMatrix';
    else
        for i = 1:nSamples-1;
            for j = i+1:min(nSamples,i+1+borderSamples*2);
                w(ii,1) = weightMatrix(i,j);
                ii = ii+1;
            end
        end
    end
    w = w*length(w)./nansum(w);
end
ii = 0;
A = zeros((nSamples-1)*(1+borderSamples*2),nSamples-1);
for i = 1:nSamples-1;
    for j = i+1:min(nSamples,i+1+borderSamples*2);
        ii = ii+1;
        A(ii,i:j-1) = 1;
    end
end
A = A(1:ii,:);
Gamma = diag([ones(nSamples-2,1);0])+diag(-1*ones(nSamples-2,1),1);
if hasJoints
    Gamma(allowJointatIndex,:) = 0.5*Gamma(allowJointatIndex,:);
    Gamma(:,allowJointatIndex) = 0.5*Gamma(:,allowJointatIndex);
end
if ~isWeighted || isempty(weightMatrix)
    multiResMatrix = (A.'*A + tikhonovScaling*(Gamma.'*Gamma))\A.';
else
    %W = diag(w);
    W = sparse(1:length(w),1:length(w),double(w));
    multiResMatrix = (A.'*W*A + tikhonovScaling*(Gamma.'*Gamma))\(A.'*W);
end