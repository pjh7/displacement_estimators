function [arfiobj,ccobj] = estimatedisp_rf(estobj,rfobj0,rfobj1)
            params = rfobj0.params;
            fs = params.fs;
            f0 = params.f0;
            c = params.c;
            kernel_lambda = estobj.kernel_lambda;
            search_lambda = estobj.search_lambda;
            if isempty(estobj.fs_out)
                fs_out = params.fs;
            else
                fs_out = estobj.fs_out;
            end
            if isempty(estobj.fs_calc)
                fs_calc = params.fs;
            else
                fs_calc = estobj.fs_calc;
            end
            rfobj0 = resample(rfobj0,fs_calc);
            rfobj1 = resample(rfobj1,fs_calc);
            if fs_out<fs_calc;
                if ((fs_calc/fs_out)-round(fs_calc/fs_out))>1e-3
                    fs_down = fs_calc/floor(fs_calc/fs_out);
                    [N D] = rat(fs_out/fs_down);
                    if N+D > rfobj0.maxResamplePQsum
                        error('Requested resample sequence (%g -> %g(%g) -> %g) uses a rational resampling rate of P/Q = %g/%g, which exceeds the limit of P+Q < %g specified by the %s object. You may set the maxResamplePQsum property to a higher value, or use a different target frequency.',fs,fs_calc,fs_down,fs_out,N,D,rfobj0.maxResamplePQsum,class(rfobj0));
                    end
                    if ((fs_calc/fs_out)-round(fs_calc/fs_out))>1e-3
                    warning('fs:ratio','fs_calc/fs_out is not an integer. Results may take a long time to resample.');
                    end
                else
                    fs_down = fs_out;
                end
                downsample_factor = round(fs_calc/fs_down);
            else
                downsample_factor = 1;
            end
            samplesPerWavelength = fs_calc/f0;
            kernel_samples = floor(kernel_lambda*samplesPerWavelength/2)*2+1;
            search_samples = floor(search_lambda*samplesPerWavelength/2)*2+1;
            tic
            sz0 = size(rfobj0.data);
            sz1 = size(rfobj1.data);
            outsz = sz1;
            outsz(1) = floor(sz1(1)/downsample_factor);
            rfdata0 = reshape(rfobj0.data,sz0(1),[],sz0(end));
            rfdata1 = reshape(rfobj1.data,sz1(1),[],sz1(end));
            if estobj.GPU
                gpu = gpuDevice;
                reset(gpu);
                wait(gpu);
                [shift,cc] = deal(zeros([outsz(1) prod(outsz(2:end-1)) outsz(end)],'single'));
                for i = 1:size(rfdata1,2);
                    rfdata0_gpu = gpuArray(single(rfdata0(:,i,:)));
                    rfdata1_gpu = gpuArray(single(rfdata1(:,i,:)));
                    [shift_gpu,cc_gpu] = calc_normxcorr_GPU(rfdata0_gpu,rfdata1_gpu,kernel_samples,search_samples,downsample_factor);
                    wait(gpu);
                    clear rfdata0_gpu rfdata1_gpu
                    shift(:,i,:) = gather(shift_gpu);
                    cc(:,i,:) = gather(cc_gpu);
                end
                reset(gpu);
            else
                [shift,cc] = deal(zeros([outsz(1) prod(outsz(2:end-1)) outsz(end)],class(rfobj1.data)));
                if estobj.disableMex
                    calc_disp = @calc_normxcorr_matlab;
                else
                    calc_disp = @calc_normxcorr;
                end
                if estobj.disableParallel
                    barsz = min(size(rfdata1,2),25);
                    barn = size(rfdata1,2);
                    fprintf(sprintf('[%%-%gs]\n',barsz),'');
                    for i = 1:size(rfdata1,2);
                        fprintf([8*ones(1,barsz+3) sprintf(sprintf('[%%-%gs]\n',barsz),'.'*ones(1,round(barsz*i/barn)))]);
                        [shift(:,i,:),cc(:,i,:)] = calc_disp(rfdata0(:,i,:),rfdata1(:,i,:),kernel_samples,search_samples,downsample_factor);
                        
                    end
                else
                    if size(rfdata1,2) == 1
                        parfor i = 1:size(rfdata1,3)
                            [shift(:,1,i),cc(:,1,i)] = calc_disp(rfdata0(:,1,i),rfdata1(:,1,i),kernel_samples,search_samples,downsample_factor);
                        end
                    else
                        parfor i = 1:size(rfdata1,2)
                            [shift(:,i,:),cc(:,i,:)] = calc_disp(rfdata0(:,i,:),rfdata1(:,i,:),kernel_samples,search_samples,downsample_factor);
                        end
                    end
                end
            end
            shift = reshape(shift,outsz);
            cc = reshape(cc,outsz);
            t1 = toc;
            arfiobj = datacontainer(rfobj1);
            arfiobj.log = char(arfiobj.log,sprintf('Normalized Cross Correlation, %g lambda (%g sample) kernel, %g lambda (%g sample) search region (%0.3fs)',estobj.kernel_lambda,kernel_samples,estobj.search_lambda,search_samples,t1));
            arfiobj.data = 1e6*shift*((c/2)*(1/fs_calc));
            arfiobj.units = sprintf('\265m');
            arfiobj.params.disp_est = estobj;
            if downsample_factor>1
                arfiobj.params.fs = fs_down;
                x0 = arfiobj.dims.(arfiobj.dims.names{1});
                arfiobj.dims.(arfiobj.dims.names{1}) = x0(1)+diff(x0(1:2))*downsample_factor*[0:size(arfiobj.data,1)-1]';
            end
            
            if nargout>1
                ccobj = arfiobj;
                ccobj.data = cc;
                ccobj.units = 'cc\_coef';
                if ccobj.params.fs ~= fs_out
                    ccobj = interp(ccobj,1,fs_out/ccobj.params.fs);
                end
            end
            if arfiobj.params.fs ~= fs_out
                arfiobj = interp(arfiobj,1,fs_out/arfiobj.params.fs); 
            end
            switch estobj.crop
                case 'valid'
                    margin = ceil((kernel_samples-1)/2*(fs_out/fs_calc));   
                    idx = (margin+1):datasize(arfiobj,1)-margin;
                    arfiobj = arfiobj.slice(1,idx);
                    if nargout>1
                        ccobj = ccobj.slice(1,idx);
                    end
                case 'full'
            end
            
        end