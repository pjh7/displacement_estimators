classdef normxcorr < displacementestimator
    properties
        search_lambda = 1,
        search_inc_lambda = 1/32;
        fs_calc = [];
        fs_out = [];
        crop = 'valid'
        GPU = false;
    end
    properties (Hidden = true)
        disableMex = false;
        disableParallel = false;
    end
    properties (Hidden = true,SetAccess = private);
        inputType = {'rfdata','iqdata'};
    end
    methods
        function estobj = normxcorr(varargin)
            estobj = setprops(estobj,varargin);
            if ~isempty(estobj.fs_calc) && ~isempty(estobj.fs_out)
                if mod(estobj.fs_calc/estobj.fs_out,1)
                    downsample = floor(estobj.fs_calc/estobj.fs_out);
                    [N, D] = rat(estobj.fs_out/estobj.fs_calc/downsample);
                    if N+D > 1000
                        warning('fs:ratio','requested fs_out and fs_calc downsamples calculations by %g and needs a resampling ratio of P/Q = %g/%g. Computation time scales with P+Q. Results may take a long time to compute.',downsample,N,D);
                    end
                end
            end
        end
        function  [arfiobj,ccobj] = estimatedisp(estobj,dataobj0,dataobj1)
            switch class(dataobj0)
                case 'rfdata'
                    [arfiobj,ccobj] = estimatedisp_rf(estobj,dataobj0,dataobj1);
                case 'iqdata'
                    [arfiobj,ccobj] = estimatedisp_iq(estobj,dataobj0,dataobj1);
                otherwise
                    error('Undetectable data type');
            end
        end 
        [arfiobj,ccobj] = estimatedisp_rf(estobj,rfobj0,rfobj1)
        [arfiobj,ccobj] = estimatedisp_iq(estobj,iqobj0,iqobj1)
    end
end