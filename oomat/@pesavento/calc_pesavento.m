function [U,cc]= calc_pesavento(IQ0,IQ1,fs,f0,c,kernel_lambda,N,quiet)
if ~exist('kernel_lambda','var')
    kernel_lambda = 1.5;
end
if ~exist('search_lambda','var')
    search_lambda = 100;
end
if ~exist('N','var')
    N = 3;
end
if ~exist('quiet','var')
    quiet = 0;
end

%IQ0 = repmat(IQ0,size(IQ1)./size(IQ0));
for i = 1:ndims(IQ1)
if size(IQ0,i) == 1;
    rep = ones(1,ndims(IQ1));rep(i) = size(IQ1,i);
    IQ0 = repmat(IQ0,rep);
end
end
tau = zeros([1 size(IQ0)]);
U = zeros(size(IQ0));
K = size(IQ0,1);
if nargout > 1
    cc = zeros(size(IQ0));
end
kernelLength = (kernel_lambda*fs/f0);
if kernelLength<1;
    warning('kernel:toosmall',sprintf('Kernel length too small (%0.1f samples). Extending to 3 samples (%g lambda)',kernelLength,3*f0/fs));
    kernelLength = 1;
end
%searchWin = round(search_lambda*fs/f0);
kernelLags = -round(kernelLength/2):round(kernelLength/2);
%grossLags = [-ceil(searchWin):ceil(searchWin)];
%kernelLength = length(kernelLags);
tstart = tic;
if ~quiet
fprintf('Axial Sample %03.0f/%03.0f',0,size(IQ0,1))
end
parfor axidx = 1:size(IQ0,1);
    if ~quiet
        fprintf('%s%03.0f/%03.0f',8*ones(1,7),axidx,size(IQ0,1));
    end
    idx = max(1,axidx+kernelLags(1)):min(K,axidx+kernelLags(end));
    kernelLength = length(idx);
    x0 = IQ0(idx,:,:);
    mux0 = nanmean(x0,1);
    sigx0 = nanstd(x0,1,1);
    x0bZeroMean = x0-mux0(ones(kernelLength,1),:,:);
    x0bZeroMean = x0bZeroMean./sigx0(ones(kernelLength,1),:,:);
    
%     if length(grossLags)==1 && grossLags ==0;
        x1 = IQ1(idx,:,:);
        mux1 = nanmean(x1,1);
        sigx1 = nanstd(x1,1,1);
        x1bZeroMean = x1-mux1(ones(kernelLength,1),:,:,:);
        x1bZeroMean = x1bZeroMean./sigx1(ones(kernelLength,1),:,:);
        SampleShift = 0;
%     else
%         
%         for grossLagIdx = 1:length(grossLags)
%             x1 = IQ1(max(1,min(K,idx+grossLags(grossLagIdx))),:,:);
%             mux1 = nanmean(x1,1);
%             sigx1 = nanstd(conj(x1),1,1);
%             x1bZeroMean = x1-mux1(ones(Klen,1),:,:,:);
%             NCCcoeff(grossLagIdx,:,:) = nansum(conj(x1bZeroMean.*x0bZeroMean))./((Klen).*(sigx0.*sigx1));
%         end
%         [NCC grossLagPeakIdx] = max(abs(NCCcoeff),[],1);
%         SampleShift = grossLags(grossLagPeakIdx);
%         
%         x1SampleShifted = x1;
%         for j = 1:size(x1,2)
%             for k = 1:size(x1,3);
%                 idx1 = max(1,min(K,idx+SampleShift(1,j,k)));
%                 x1SampleShifted(:,j,k) = IQ1(idx1,j,k).*exp(1j*2*pi*f0*SampleShift(1,j,k)*(1/fs));
%             end
%         end
%         x1 = x1SampleShifted;
%         sigx1 = nanstd(x1SampleShifted,1,1);
%         x1bZeroMean = x1-mux1(ones(Klen,1),:,:,:);
%         x1bZeroMean = x1bZeroMean./sigx1(ones(Klen,1),:,:);
%     end
    
    corrVal = nanmean(conj(x1bZeroMean).*(x0bZeroMean),1);
    corrValPhasCrct = angle(corrVal);
    tau = corrValPhasCrct/(2*pi*f0);

    for n = 1:N-1
        x0PhaseShifted = subsampleshift(x0bZeroMean,f0,tau/2);
        x1PhaseShifted = subsampleshift(x1bZeroMean,f0,-tau/2);
        %corrVal = nansum(conj(x1PhaseShifted-repmat(nanmean(x1PhaseShifted,1),[Klen 1 1 1])).*(x0PhaseShifted-repmat(nanmean(x0PhaseShifted,1),[Klen 1 1 1])))./((Klen).*sigx1.*sigx0);
        corrVal = nanmean(conj(x1PhaseShifted).*(x0PhaseShifted),1);
        corrValPhasCrct = angle(exp(-1j*2*pi*f0*(tau)/fs).*corrVal);
        tau = tau + corrValPhasCrct/(2*pi*f0);
    end
    cc(axidx,:,:) = corrVal;
    U(axidx,:,:) = tau+(SampleShift*(1/fs));
end
U = 1*U*c/2;

if ~quiet
fprintf('...done (%0.2fs)\n',toc(tstart));
end
end

function x = subsampleshift(x0,f0,tau)
if numel(tau) == 1
    x = (ifft(fft(x0)*exp(-1j*2*pi*f0*tau)));
elseif size(tau,1) == 1;
    tau = repmat(tau,size(x0)./size(tau));
    x = (ifft(fft(x0).*exp(-1j*2*pi*f0*tau)));
else
    x = x0;
    for i =1 :size(tau,2)
        for j = 1:size(tau,3);
            for k = 1:size(tau,4);
                x(:,i,j,k) = diag(ifft(fft(x0)*exp(-1j*2*pi*f0*tau(:,i,j,k)).'));
            end
        end
    end
    
end
end