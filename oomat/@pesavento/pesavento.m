classdef pesavento < displacementestimator
    properties
        iterations = 3;
    end
    properties (Hidden = true,SetAccess = private);
        inputType = 'iqdata';
    end
    methods
        function estobj = pesavento(varargin)
            %estobj.options = addfields(estobj.options,varargin);
            estobj = setprops(estobj,varargin);
        end
        function  [arfiobj,ccobj] = estimatedisp(estobj,iqobj0,iqobj1)
            params = estobj.params;
            fs = params.fs;
            f0 = params.f0;
            c = params.c;
            kernel_lambda = estobj.kernel_lambda;
            iter = estobj.iter;
            [u,cc] = pesavento.calc_pesavento(iqobj0.data,iqobj1.data,fs,f0,c,kernel_lambda,iterations,1);
            arfiobj = datacontainer(iqobj1);
            arfiobj.data = 1e6*u;
            arfiobj.units = sprintf('\265m');
            arfiobj.params.disp_est = estobj;
            if nargout>1
                ccobj = arfiobj;
                ccobj.data(:) = cc(:);
                ccobj.units = 'cc_coef';
            end
        end
        function msg = log(estobj)
            msg = sprintf('Pesavento''s Phase-Zero Estimation Algorithm, k = %0.2g wavelengths, %g iterations',obj.options.kernel_lambda,obj.options.iterations);
        end
    end
    methods (Static)
        [u,cc]= calc_pesavento(IQ0,IQ1,fs,f0,c,kernel_lambda,search_lambda,N,quiet)
    end
end