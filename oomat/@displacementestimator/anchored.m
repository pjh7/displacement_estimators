 function [arfiobj, ccobj] = anchored(estobj,dataobj,varargin)
        defaults = struct('refidx',1,'dim','t');
        dataobj = conditionInput(estobj,dataobj);
        options = addfields(defaults,varargin);
        if ischar(options.dim) && strcmp(options.dim,'t') && ~isdim(dataobj,'t') 
            warning('t:notfound','Could not find ''t'' dimension, Using last dimension (%s) by default',dataobj.dims.names{end});
            options.dim = ndims(dataobj);
        end
        [dim,dimname] = getdim(dataobj,options.dim);
        if dim ~= ndims(dataobj)
            dataobj = permute(dataobj,[1:dim-1 dim+1:ndims(dataobj) dim]);
            permuted = true;
            unpermute = [1:dim-1 ndims(dataobj), dim:ndims(dataobj)-1];
        else
            permuted = false;
        end
        [dim,dimname] = getdim(dataobj,dimname);
        dataobj.log = char(dataobj.log,datestr(now),sprintf('Displacement estimation anchored to frame %g',options.refidx));
        refobj = slice(dataobj,dim,options.refidx*ones(1,length(dataobj.dims.(dimname))));  
        if nargout>1
            [arfiobj,ccobj] = estimatedisp(estobj,refobj,dataobj);
            if permuted
                arfiobj = permute(arfiobj,unpermute);
                ccobj = permute(ccobj,unpermute);
            end
        else
            arfiobj = estimatedisp(estobj,refobj,dataobj);
            if permuted
                arfiobj = permute(arfiobj,unpermute);
            end
        end
        
        
end