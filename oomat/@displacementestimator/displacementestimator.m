classdef displacementestimator
    properties (Abstract,Hidden,SetAccess=private)
        inputType
    end
    properties
        kernel_lambda = 1.5;
    end
    methods (Abstract,Hidden)
        arfi = estimatedisp(obj,ref,tgt,params)
    end

    methods (Hidden)
        function dataobj = conditionInput(estobj,dataobj)
            left = @(x,n)x(1:end-n);
            if ~any(strcmp(class(dataobj),estobj.inputType));
                if iscell(estobj.inputType)
                    error('Data are of class %s. %s only accepts data of classes %s.',class(dataobj),class(estobj),left(sprintf('%s or ',estobj.inputType{:}),4));
                else
                    error('Data are of class %s. %s only accepts data of class %s.',class(dataobj),class(estobj),estobj.inputType);
                end

            end
%             if ~isa(dataobj,estobj.inputType)
%                 switch class(dataobj)
%                     case estobj.inputType
%                     case 'iqdata'
%                         dataobj = demodulate(dataobj);
%                     case 'rfdata'
%                         dataobj = remodulate(dataobj);
%                 end
%             end
        end
    end
    methods
        function varargout = estimate(estobj,dataobj,reference_scheme,varargin)
            if ~any(strcmpi(methods('estimator'),reference_scheme))
                error('unknown reference scheme %s',reference_scheme)
            end
            [varargout{1:nargout}] = estobj.(reference_scheme)(dataobj,varargin);
        end 
        
        function propstruct = getprops(estobj)
            propnames = properties(estobj);
            for i = 1:length(propnames)
                propstruct.(propnames{i}) = estobj.(propnames{i});
            end
        end
        
        [arfi,cc] = anchored(obj,usobj,varargin)
        [arfi,cc] = progressive(obj,usobj,varargin)
        [arfi,cc] = multiresolution(obj,usobj,varargin)
        
        function [crlb]  = cramerraolowerbound(obj,usobj,rho,SNRdB)
            if ~exist('rho','var')
                rho = .999;
            end
            if ~exist('SNRdB','var')
                SNRdB = inf;
            end
            f0 = usobj.centerfrequency;
            B = diff(usobj.bandwidth(-6))/f0;
            T = obj.kernel_lambda * (1/usobj.params.f0);
            sigma = sqrt(3/(2*f0^3*pi^2*T*(B^3 + 12*B))*(1/(rho^2)*(1+1/(SNRdB^2))^2-1));
            crlb = sigma * usobj.params.c/2;
        end
    end
end