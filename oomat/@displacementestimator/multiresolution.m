 function [arfiobj, ccobj] = multiresolution(estobj,dataobj,varargin)
defaults = struct('refidx',1,'dim','t','skipidx',[],'interp',true,'borderSamples',inf','tikhonovScaling',0,'weightingScheme','none','snrClip',inf);
dataobj = conditionInput(estobj,dataobj)    ;
options = addfields(defaults,varargin);
if ischar(options.dim) && strcmp(options.dim,'t') && ~isdim(dataobj,'t') 
    warning('t:notfound','Could not find ''t'' dimension, Using last dimension (%s) by default',dataobj.dims.names{end});
    options.dim = ndims(dataobj);
end
[dim,dimname] = getdim(dataobj,options.dim);
if dim == 1
    error('cannot reference along the same direction as the search')
end
if dim ~= 2
    dataobj = permute(dataobj,[1 dim 2:dim-1 dim+1:ndims(dataobj)]);
end
sz = size(dataobj.data);
useidx = setxor(1:sz(2),options.skipidx);
% if sum(size(dataobj.data)>1)>3
%     error('can''t handle more than three dimensions for now.');
% end
nx = size(dataobj.data(:,:,:),3);
I = [];
J = [];
ii = 1;
dataobj1 = slice(dataobj,2,useidx);
nTimesteps = size(dataobj1.data,2);
t = dataobj1.dims.(dataobj.dims.names{2});
for i = 1:nTimesteps-1;
    for j = i+1:min(nTimesteps,i+1+options.borderSamples*2);
        I(ii) = i;
        J(ii) = j;
        ii = ii+1;
    end
end
for xidx = 1:nx;
%fprintf('x(%g/%g):',xidx,nx);
xobj = dataobj1(:,:,xidx);
refobj = slice(xobj,2,I);
tgtobj = slice(xobj,2,J);
%fprintf('displacements...');
[arfiobj, ccobj] = estimatedisp(estobj,refobj,tgtobj);
if xidx == 1
    arfidata0 = zeros(size(arfiobj.data,1),nTimesteps-1,nx);
end
%fprintf('done. Multires...');
switch lower(options.weightingScheme)
    case 'meansnr'
                snr = max(0,min(options.snrClip,squeeze(mean(ccobj.data(:,:))./(1-mean(ccobj.data(:,:))))));
                multiResMatrix = calcMultiResMatrix(nTimesteps,options.borderSamples,options.tikhonovScaling,snr);
                data = arfiobj.data;
            for zidx = 1:size(arfiobj.data,1)
                 arfidata0(zidx,:,xidx) = solveMultiRes((data(zidx,:)'),multiResMatrix,options.borderSamples);
            end
    case 'snr'
                snr = max(0,min(options.snrClip,squeeze(ccobj.data(:,:)./(1-(ccobj.data(:,:))))));
                data = arfiobj.data;
            for zidx = 1:size(arfiobj.data,1)
                multiResMatrix = calcMultiResMatrix(nTimesteps,options.borderSamples,options.tikhonovScaling,snr(zidx,:)');
                arfidata0(zidx,:,xidx) = solveMultiRes((data(zidx,:)'),multiResMatrix,options.borderSamples);
            end
            
    case 'none'
                multiResMatrix = calcMultiResMatrix(nTimesteps,options.borderSamples,options.tikhonovScaling);
            for zidx = 1:size(arfiobj.data,1)
                arfidata0(zidx,:,xidx) = solveMultiRes((arfiobj.data(zidx,:)'),multiResMatrix,options.borderSamples);
            end
        
    otherwise
            error('unknown weighting scheme %s',options.weightingScheme);
end
%fprintf('done\n');
end

if ~isempty(options.skipidx);
    s = sprintf(' %g',options.skipidx);
    skipstr = sprintf(', skipping frames [%s]',s(2:end));
else
    skipstr = '';
end
tgtobj.log = char(tgtobj.log,datestr(now),sprintf('Progresive displacement estimation, zeroing on frame %g%s',options.refidx,skipstr));
arfidata = single(nan([size(arfiobj.data,1) sz(2:end)]));
arfidata(:,useidx,:) = reshape(cumsum(cat(2,0*arfidata0(:,1,:),arfidata0),2),size(arfidata0,1),length(useidx),[]);
if nargout>1
    cc = single(nan(size(ccobj.data)));
    cc(:,useidx,:) = ccobj.data;
    ccobj.data = cc;
end
%clear darfidata
if options.interp && ~isempty(options.skipidx)
    for i = 1:length(options.skipidx);
        idx = options.skipidx(i);
        prioridx = useidx(useidx<idx);
        nextidx = useidx(useidx>idx);
        if isempty(prioridx)
            arfidata(:,idx,:) = arfidata(:,nextidx(1),:)-(arfidata(:,nextidx(2),:)-arfidata(:,nextidx(1),:))*(nextidx(1)-idx)/(nextidx(2)-nextidx(1));
        elseif isempty(nextidx)
            arfidata(:,idx,:) = arfidata(:,prioridx(end),:)+(arfidata(:,prioridx(end),:)-arfidata(:,prioridx(end-1),:))*(idx-prioridx(end))/(prioridx(end)-prioridx(end-1));
        else
            frac = (idx-prioridx(end))/(nextidx(1)-prioridx(end));
            arfidata(:,idx,:) = arfidata(:,prioridx(end),:) + frac*(arfidata(:,nextidx(1),:)-arfidata(:,prioridx(end),:));
        end
    end
end
if options.refidx > 1;
    arfidata(:,:,:) = arfidata(:,:,:)-arfidata(:,options.refidx*ones(1,size(arfidata,2)),:);
end
arfiobj.data = arfidata;
arfiobj.dims.t = dataobj.dims.t;
for i = 3:ndims(dataobj)
arfiobj.dims.(dataobj.dims.names{i}) = dataobj.dims.(dataobj.dims.names{i});
end
if dim ~= 2
    arfiobj = permute(arfiobj,[1 3:dim 2 dim+1:ndims(arfiobj)]);
end

%arfi.dims.(arfi.dims.names{1}) = z;
end