function [arfiobj,ccobj] = progressive(estobj,dataobj,varargin)
defaults = struct('refidx',1,'dim','t','skipidx',[],'interp',true);
dataobj = conditionInput(estobj,dataobj);
options = addfields(defaults,varargin);
if ischar(options.dim) && strcmp(options.dim,'t') && ~isdim(dataobj,'t') 
    warning('t:notfound','Could not find ''t'' dimension, Using last dimension (%s) by default',dataobj.dims.names{end});
    options.dim = ndims(dataobj);
end
[dim,dimname] = getdim(dataobj,options.dim);
if dim == 1
    error('cannot reference along the same direction as the search')
end
if ~isempty(options.skipidx);
    skipidxstr = sprintf(' %g',options.skipidx);
    skipstr = sprintf(', skipping frames [%s]',skipidxstr(2:end));
else
    skipstr = '';
end
dataobj.log = char(dataobj.log,datestr(now),sprintf('Progresive displacement estimation, zeroing on frame %g%s',options.refidx,skipstr));
useidx = setxor(1:size(dataobj.data,dim),options.skipidx);
ndim = ndims(dataobj);
if dim ~= ndim
    dataobj = permute(dataobj,[1:dim-1 dim+1:ndim-1 ndim]);
end
sz = size(dataobj.data);
refobj = slice(dataobj,ndim,useidx([1 1:end-1]));
tgtobj = slice(dataobj,ndim,useidx);
if nargout>1
    [arfiobj, ccobj] = estimatedisp(estobj,refobj,tgtobj);
else
    [arfiobj] = estimatedisp(estobj,refobj,tgtobj);
end
outsz = sz;
outsz(1) = size(arfiobj.data,1);
%[darfidata, cc] = estimatedisp(obj,refdata,tgtdata,dataobj.params);
arfidata = single(nan([size(arfiobj.data,1) prod(sz(2:end-1)) sz(end)]));
arfidata(:,:,useidx) = reshape(cumsum(arfiobj.data,ndim),size(arfiobj.data,1),[],length(useidx));

if nargout>1
    cc = single(nan(size(arfidata)));
    cc(:,:,useidx) = ccobj.data;
    ccobj.data = reshape(cc,outsz);
    if dim ~= ndim
        ccobj = permute(ccobj,[1:dim-1 ndim dim+1:ndim-1]);
    end
    ccobj.dims.(dimname) = dataobj.dims.(dimname);
end
%clear darfidata
if options.interp && ~isempty(options.skipidx)
    for i = 1:length(options.skipidx);
        idx = options.skipidx(i);
        prioridx = useidx(useidx<idx);
        nextidx = useidx(useidx>idx);
        if isempty(prioridx)
            arfidata(:,:,idx) = arfidata(:,:,nextidx(1))-(arfidata(:,:,nextidx(2))-arfidata(:,:,nextidx(1)))*(nextidx(1)-idx)/(nextidx(2)-nextidx(1));
        elseif isempty(nextidx)
            arfidata(:,:,idx) = arfidata(:,:,prioridx(end))+(arfidata(:,:,prioridx(end))-arfidata(:,:,prioridx(end-1)))*(idx-prioridx(end))/(prioridx(end)-prioridx(end-1));
        else
            frac = (idx-prioridx(end))/(nextidx(1)-prioridx(end));
            arfidata(:,:,idx) = arfidata(:,:,prioridx(end)) + frac*(arfidata(:,:,nextidx(1))-arfidata(:,:,prioridx(end)));
        end
    end
    arfiobj.log = char(arfiobj.log,sprintf('Interpolated skipped indices [%s]',skipidxstr(2:end)));
end
if options.refidx > 1;
    arfidata(:,:,:) = arfidata(:,:,:)-arfidata(:,:,options.refidx*ones(1,size(arfidata,3)));
end
arfiobj.data = reshape(arfidata,outsz);
arfiobj.dims.(dimname) = dataobj.dims.(dimname);
    if dim ~= ndim
        arfiobj = permute(arfiobj,[1:dim-1 ndim dim:ndim-1]);
    end
end