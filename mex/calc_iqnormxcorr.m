%CALC_IQNORMXCORR complex IQ normalized cross correlation estimator (MEX)
%[shift cc corr] = CALC_IQNORMXCORR(iq_ref,iq_tgt,fs,f0,fdem,ksz_lambda,srch_max_lambda,srch_inc_lambda)
%     
% CALC_IQNORMXCORR computes the normalized cross correlation on IQ data,
% using frequency domain time shifts to test time delays.
%-------------------------------------------------------------------------
% INPUTS
%   iq_ref: N-D matrix containing the reference IQ data. May be single
%       or double precision, but must match iq_tgt. A single frame can be
%       used to match multiple iq_tgt frames, but the dimension in which
%       the reference data will be repeated must be the last dimension.
%       Example: iq_ref can be [512x64x1] and iq_tgt can be [512x64x30],
%       but [512x64x1x32] and [512x64x30x32] won't work (permute to
%       [512x64x32x1] and [512x64x32x30] for intended operation.
%
%   iq_tgt: N-D matrix containing the target IQ data. May be single or
%       double precision, but must match iq_ref. 
%
%   fs: the sampling frequency of the data (Hz)
%   
%   f0: the center frequency of the data (Hz)
%
%   fdem: the demodulation frequency of the data (Hz). Typically -f0 for
%       baseband iqdata or 0 for hilbert-transformed data
%   
%   ksz_lambda: axial kernel size in wavelengths.
%
%   srch_max_lambda: axial search window size in wavelengths.
%
%   srch_inc_lambda: axial search increment in wavelengths.
%
%-------------------------------------------------------------------------
% OUTPUTS
%   shift: calculated shift between iq_ref and iq_tgt (time). Same
%       size as rf_tgt. 
%      cc: peak correlation coefficient (complex)
%    corr: raw correlation function (complex)
%-------------------------------------------------------------------------

% Version 0.1
%
% 2015/07/07 Peter Hollender
%      Mexified code.
%
% In order to compile this code, use the following command:
%
% mex -v calc_normxcorr.cpp
function varargout = calc_iqnormxcorr(varargin)
if exist('calc_iqnormxcorr_mex')==3
    if nargout == 1
        varargout{1} = calc_iqnormxcorr_mex(varargin{:});
    else
        [varargout{1},varargout{2}] = calc_iqnormxcorr_mex(varargin{:});
    end
else
    [pth,me,ext] = fileparts(mfilename);
    %warning('path:nomex','mex file %s.%s not found. Using %s_matlab.m. Consider compiling src_mex/%s.cpp',me,mexext,me,me);
    if nargout == 1
        varargout{1} = calc_iqnormxcorr_matlab(varargin{:});
    else
        [varargout{1},varargout{2}] = calc_iqnormxcorr_matlab(varargin{:});
    end
end
