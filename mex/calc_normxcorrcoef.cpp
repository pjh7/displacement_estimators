#include "mex.h"
#include "matrix.h"
#include <math.h>

/* Input arguments */
#define RF_REF          prhs[0]
#define RF_TGT          prhs[1]
#define KSZ_SAMP    prhs[2]
#define DECIMATIONFACTOR prhs[3]

/* Output arguments */
#define CC_OUT        plhs[0]

// Define constants
#define PI 3.14159265

template<typename Type>
void normxcorrcoef(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    /* Check number of input arguments */
    if (nrhs < 3)
        mexErrMsgTxt("Not enough input arguments.");
		
    /* Declare indexing variables */
    long int idxtgt,outidx,minidx,sidx,kidxr,kidxt,si,ki;
	
	/* Get input/output matrix sizes */
	long int decimationfactor,insz0;
    mwSize *outsz;
	long int ksz_samp	= (long int)mxGetScalar(KSZ_SAMP);
    //long int srch_samp	= (long int)mxGetScalar(SRCH_SAMP);
    size_t numel_tgt = mxGetNumberOfElements(RF_TGT);
    size_t numel_ref = mxGetNumberOfElements(RF_REF);
    if (nrhs==4)
        decimationfactor = 1;
    else
        decimationfactor = (int)mxGetScalar(DECIMATIONFACTOR);
    outsz = (mwSize*)mxGetDimensions(RF_TGT);
    insz0 = outsz[0];
    outsz[0] = outsz[0]/decimationfactor;
    
	/* Check kernel and search sizes against allocated buffers */
	const long int max_kernel = 1024;
    const long int max_search = 1;
	if (ksz_samp>max_kernel){
        mexPrintf("Error: Requested kernel size is set to %d samples. Maximum kernel size is %d samples.\n",ksz_samp,max_kernel);
        mexErrMsgTxt("Maximum kernel size exceeded.");
    }
		/* Declare I/O pointers */
		Type *ref, *tgt, *cc_out;
		/* Declare variables */
		Type mean_ref,sumSq_ref,mean_tgt,sumSq_tgt,mindot,ssa,ssb,ssc,ssd;
		Type ssx[3];
		Type ssy[3];
		Type dot_prod;
		/* Declare kernel buffer */
		Type Kref[max_kernel];
		Type Ktgt[max_kernel];
		/* Declare search region buffer */
		Type SrchWin_ref[max_kernel];
		Type SrchWin_tgt[max_kernel];
		CC_OUT = mxCreateNumericArray(mxGetNumberOfDimensions(RF_TGT),outsz,mxGetClassID(prhs[1]),mxREAL);
		cc_out  = (Type*)mxGetData(CC_OUT);
		ref       = (Type*)mxGetData(RF_REF);
		tgt       = (Type*)mxGetData(RF_TGT);
			/* This code snippet is the functional for-loop for calc_normxcorr.cpp. 
	It lives in a separate file so that it is read twice at compilation via an include
	statement, allowing it to run for both single and double inputs.
	*/
	
	/* Loop through target matrix */
    for(idxtgt=(0);idxtgt<(numel_tgt);idxtgt++)
    {
	//mexPrintf("idxtgt = %d, ",idxtgt);
	    if(idxtgt==0){
			/* Initialize running averages */
			mean_ref = 0;
            mean_tgt = 0;
			sidx = idxtgt;
		//	mexPrintf("[");
			/* Accumulate first running average*/
			for (ki = 0;ki<ksz_samp;ki++){
				kidxr = ((idxtgt - (ksz_samp/2) + ki + numel_ref) % numel_ref);
				kidxt = ((idxtgt - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
				mean_ref += ref[kidxr]/ksz_samp;
				mean_tgt += tgt[kidxt]/ksz_samp;
			//	mexPrintf("%g ",(double)ref[kidxt]);
			}
			//mexPrintf("] mu0 = %g mu1 = %g ",(double)mean_ref,(double)mean_tgt);
			sumSq_ref = 0;
			sumSq_tgt = 0;
			//mexPrintf("[");
			for (ki = 0;ki<ksz_samp;ki++){	
				kidxr = ((idxtgt - (ksz_samp/2) + ki + numel_ref) % numel_ref);
				kidxt = ((idxtgt - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
				Kref[ki] = ref[kidxr]-mean_ref;
				Ktgt[ki] = tgt[kidxt]-mean_tgt;
				sumSq_ref += Kref[ki]*Kref[ki];
				sumSq_tgt += Ktgt[ki]*Ktgt[ki];
			//	mexPrintf("%g ",(double)Kref[ki]);
			}
			//mexPrintf("] sigt = %g ",(double)sumSq_tgt);
			//mexPrintf("[");
			for (ki = 0;ki<ksz_samp;ki++){
			/* Normalize by L2-Norm and store in circular buffer */
				SrchWin_ref[ki] = Kref[ki]/sqrt(sumSq_ref/ksz_samp);
				SrchWin_tgt[ki] = Ktgt[ki]/sqrt(sumSq_tgt/ksz_samp);
			//	mexPrintf("%g ",(double)SrchWin_tgt[ki]);
			}
			//mexPrintf("]");
        }
        else {
			/* Increment running average */
			sidx = idxtgt-1;
			ki = -1;
            kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
			kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
			mean_ref -= (ref[kidxr]/ksz_samp);
            mean_tgt -= (tgt[kidxt]/ksz_samp);
            ki = ksz_samp-1;
            kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
			kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
			mean_ref += (ref[kidxr]/ksz_samp);
            mean_tgt += (tgt[kidxt]/ksz_samp);
            /* Build new zero-mean kernel */
			sumSq_ref = 0;
            sumSq_tgt = 0;
            for (ki = 0;ki<ksz_samp;ki++){
                kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
				kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
				Kref[ki] = ref[kidxr]-mean_ref;
                Ktgt[ki] = tgt[kidxt]-mean_tgt;
                sumSq_ref += Kref[ki]*Kref[ki];
                sumSq_tgt += Ktgt[ki]*Ktgt[ki];
            }
			/* Normalize by L2-Norm and store in circular buffer*/
            for (ki = 0;ki<ksz_samp;ki++){
            SrchWin_ref[ki] = Kref[ki]/sqrt(sumSq_ref/ksz_samp);
            SrchWin_tgt[ki] = Ktgt[ki]/sqrt(sumSq_tgt/ksz_samp);
            }
        }
		if (((idxtgt % insz0) % decimationfactor)==0) {
			outidx = (idxtgt % insz0)/decimationfactor + outsz[0]*(idxtgt/insz0);
			//mexPrintf("outidx = %d ",outidx);
			dot_prod = 0;
			for (ki = 0;ki<ksz_samp;ki++){
				//mexPrintf("%g x %g,",(double)SrchWin_ref[ki],(double)SrchWin_tgt[ki]);
				dot_prod+= SrchWin_ref[ki]*SrchWin_tgt[ki];
			}
			cc_out[outidx] = dot_prod/ksz_samp;
        }
		//mexPrintf(" \n");
    }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
    /* Check Input arguments */
	if (nrhs < 4) {
        mexErrMsgTxt("Not enough input arguments.");
    }
	if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS && mxGetClassID(prhs[1]) == mxDOUBLE_CLASS) {
	 normxcorrcoef<double >(nlhs,plhs,nrhs,prhs);
	}
    else if (mxGetClassID(prhs[0]) == mxSINGLE_CLASS && mxGetClassID(prhs[1]) == mxSINGLE_CLASS) { 
	 normxcorrcoef<float >(nlhs,plhs,nrhs,prhs);
	}
	else
		mexErrMsgTxt("RF_REF and RF_TGT must be the same type, and both single or double.");	
	return;
}