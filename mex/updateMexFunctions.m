clear functions
extensions = {'.mexw64','.mexa64'};
sourcedir = fileparts(mfilename('fullpath'));
destdir = fileparts(sourcedir);
fdiff = @(file1,file2)~javaMethod('contentEquals','org.apache.commons.io.FileUtils',javaObject('java.io.File',file1),javaObject('java.io.File',file2));
for ext_idx = 1:length(extensions)
    d0 = dir(fullfile(sourcedir,['*' extensions{ext_idx}]));
    files0 = {d0.name};
    for i = 1:length(d0)
        sourcefile = fullfile(sourcedir,files0{i});
        destfile = fullfile(destdir,files0{i});
        if ~exist(destfile,'file')
            fprintf('%s -> %s\n',sourcefile,destfile)
            copyfile(sourcefile,destfile);
        elseif fdiff(sourcefile,destfile)
            fprintf('%s => %s\n',sourcefile,destfile)
            copyfile(sourcefile,destfile);
        else
            fprintf('%s == %s\n',sourcefile,destfile)
        end
    end
end
        
    