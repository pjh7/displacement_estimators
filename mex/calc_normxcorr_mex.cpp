
#include "mex.h"
#include "matrix.h"
#include <math.h>
#include "../cpp/normxcorr_engine.cpp"

/* Input arguments */
#define RF_REF          prhs[0]
#define RF_TGT          prhs[1]
#define KSZ_SAMP    prhs[2]
#define SRCH_SAMP prhs[3]
#define DECIMATIONFACTOR prhs[4]

/* Output arguments */
#define SHIFT_OUT        plhs[0]
#define CC_OUT			plhs[1]
//#define MAX_KERNEL 512
//#define MAX_SEARCH 512
// Define constants
#define PI 3.14159265

template<typename Type>
void normxcorr(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    /* Check number of input arguments */
    if (nrhs < 4)
        mexErrMsgTxt("Not enough input arguments.");
		
    /* Declare indexing variables */
    
	int idx;
	/* Get input/output matrix sizes */
	int decimationfactor;
	const long int ksz_samp	= (long int)mxGetScalar(KSZ_SAMP);
    const long int srch_samp	= (long int)mxGetScalar(SRCH_SAMP);
    const mwSize numel_tgt = mxGetNumberOfElements(RF_TGT);
    const mwSize numel_ref = mxGetNumberOfElements(RF_REF);
	const mwSize ndim_tgt = mxGetNumberOfDimensions(RF_TGT);
	const mwSize* size_tgt = mxGetDimensions(RF_TGT);
    bool calc_cc = false;
    if (nrhs==4)
        decimationfactor = 1;
    else
        decimationfactor = (int)mxGetScalar(DECIMATIONFACTOR);
		
	mwSize *outsz = (mwSize*)malloc(sizeof(mwSize)*ndim_tgt);
	for(idx=(0);idx<(ndim_tgt);idx++)
		outsz[idx] = size_tgt[idx];
	const mwSize numel_aline = outsz[0];
    outsz[0] = outsz[0]/decimationfactor;
    
	/* Check kernel and search sizes against allocated buffers */
	
/* 	if (ksz_samp>MAX_KERNEL){
        mexPrintf("Error: Requested kernel size is set to %d samples. Maximum kernel size is %d samples.\n",ksz_samp,MAX_KERNEL);
        mexErrMsgTxt("Maximum kernel size exceeded.");
    } */
    if (ksz_samp<2){
        mexPrintf("Error: Kernel must be at least two samples.");
        mexErrMsgTxt("Kernel size too small.");
    }
/*     if (srch_samp>MAX_SEARCH){
        mexPrintf("Error: Requested search window is set to %d samples. Maximum search window is %d samples.",srch_samp,MAX_SEARCH);
        mexErrMsgTxt("Maximum search window size exceeded.");
    } */
    if (srch_samp<1){
        mexPrintf("Error: Requested search window must be at least 1 sample.");
        mexErrMsgTxt("Zero search window.");
    }

    
		/* Declare I/O pointers */
		Type *ref, *tgt, *shift, *cc_out;
		/* Declare variables */

		SHIFT_OUT = mxCreateNumericArray(mxGetNumberOfDimensions(RF_TGT),outsz,mxGetClassID(RF_TGT),mxREAL);
		shift  = (Type*)mxGetData(SHIFT_OUT);
		if (nlhs == 2){
			CC_OUT = mxCreateNumericArray(mxGetNumberOfDimensions(RF_TGT),outsz,mxGetClassID(RF_TGT),mxREAL);
			cc_out  = (Type*)mxGetData(CC_OUT);
            calc_cc = true;
		}
		ref       = (Type*)mxGetData(RF_REF);
		tgt       = (Type*)mxGetData(RF_TGT);
			/* This code snippet is the functional for-loop for calc_normxcorr.cpp. 
	It lives in a separate file so that it is read twice at compilation via an include
	statement, allowing it to run for both single and double inputs.
	*/
	
	normxcorr_engine(shift,cc_out,ref,tgt,numel_ref,numel_tgt,numel_aline,ksz_samp,srch_samp,calc_cc,decimationfactor);
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
    /* Check Input arguments */
	if (nrhs < 4) {
        mexErrMsgTxt("Not enough input arguments.");
    }
	if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS && mxGetClassID(prhs[1]) == mxDOUBLE_CLASS) {
	 normxcorr<double >(nlhs,plhs,nrhs,prhs);
	}
    else if (mxGetClassID(prhs[0]) == mxSINGLE_CLASS && mxGetClassID(prhs[1]) == mxSINGLE_CLASS) { 
	 normxcorr<float >(nlhs,plhs,nrhs,prhs);
	}
	else
		mexErrMsgTxt("RF_REF and RF_TGT must be the same type, and both single or double.");	
	return;
}
