%CALC_NORMXCORR unitless normalized cross correlation estimator (MEX)
%[shift cc] = CALC_NORMXCORR(rf_ref,rf_tgt,ksz_samp,srch_samp,[decimationfactor]);
%     
% CALC_NORMXCORR is a mex-ified implementation (c++) of normalized cross correlation for the estimation of motion in ultrasound
% data. It strings out the data into 1-D vectors for rapid computation,
% which applies a quasi-circular boundary condition. You can remove these
% effects by throwing out the top and bottom half-kernels of data after
% processing. CALC_NORMXCORR uses a symmetric referencing search, creating
% search windows in both the reference and target data and sliding them
% past each other, rather than searching for a fixed template from the
% reference in the target data. This is necessary for multiresolution
% estimation because it automatically enforces Uij = -Uji.
%-------------------------------------------------------------------------
% INPUTS
%   rf_ref: N-D matrix containing the reference RF data. May be single
%       or double precision, but must match iq_tgt. A single frame can be
%       used to match multiple iq_tgt frames, but the dimension in which
%       the reference data will be repeated must be the last dimension.
%       Example: iq_ref can be [512x64x1] and iq_tgt can be [512x64x30],
%       but [512x64x1x32] and [512x64x30x32] won't work (permute to
%       [512x64x32x1] and [512x64x32x30] for intended operation.
%
%   rf_tgt: N-D matrix containing the target RF data. May be single or
%       double precision, but must match rf_ref. 
%
%   ksz_samp: integer axial kernel size. round(ksz_lambda*fs/f0) is a good
%       way to calculate it.
%
%   srch_samp: integer axial search window size. round(srch_lambda*fs/f0) is a good
%       way to calculate it.
%
%   decimationfactor: integer downscaling factor for output. If this number is larger than srch_samp, the extra kernels that aren't used will still be calculated, resulting in sub-optimal efficiency (but valid results, nonetheless). Defaults to 1 if not included.
%-------------------------------------------------------------------------
% OUTPUTS
%   shift: calculated shift between rf_ref and rf_tgt (samples). Same
%       size as rf_tgt. To get to displacements in meters, use the
%       following: u[m] = shift * (c[m/s]/2) * (1/fs[Hz])
%-------------------------------------------------------------------------

% Version 0.1
%
% 2015/07/07 Peter Hollender
%      Mexified code.
%
% In order to compile this code, use the following command:
%
% mex -v calc_normxcorr.cpp
function varargout = calc_normxcorr(varargin)
if exist('calc_normxcorr_mex')==3
    if nargout == 1
        varargout{1} = calc_normxcorr_mex(varargin{:});
    else
        [varargout{1},varargout{2}] = calc_normxcorr_mex(varargin{:});
    end
else
    [pth,me,ext] = fileparts(mfilename);
    warning('path:nomex','mex file %s.%s not found. Using %s_matlab.m. Consider compiling src_mex/%s.cpp',me,mexext,me,me);
    if nargout == 1
        varargout{1} = calc_normxcorr_matlab(varargin{:});
    else
        [varargout{1},varargout{2}] = calc_normxcorr_matlab(varargin{:});
    end
end
