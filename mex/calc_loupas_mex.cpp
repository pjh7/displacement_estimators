//#include "calc_loupas.h"
#include "mex.h"
#include "matrix.h"
#include <math.h>
#include "../cpp/loupas_engine.cpp"

/* Input arguments */
#define IQ_REF          prhs[0]
#define IQ_TGT          prhs[1]
#define KSZ_SAMP    prhs[2]
#define FDEMOVERFS prhs[3]
#define DECIMATIONFACTOR prhs[4]
#define TGT_AUTOCORR_FLAG prhs[5]

/* Output arguments */
#define PHI_OUT        plhs[0]

// Define constants
#define PI 3.14159265

template<typename Type,typename fsType>
void loupas(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

    int idx;
    const mwSize numel_tgt = mxGetNumberOfElements(IQ_TGT);
    const mwSize numel_ref = mxGetNumberOfElements(IQ_REF);
    const mwSize numel_fdem = mxGetNumberOfElements(FDEMOVERFS);
	const mwSize ndim_tgt = mxGetNumberOfDimensions(IQ_TGT);
	const mwSize* size_tgt = mxGetDimensions(IQ_TGT);

    int decimationfactor;
    if (nrhs<5)
        decimationfactor = 1;
	else
        decimationfactor = (int)mxGetScalar(DECIMATIONFACTOR);
    
    int tgt_autocorr_flag;
    if (nrhs<6)
        tgt_autocorr_flag = 1;
    else
        tgt_autocorr_flag = (int)mxGetScalar(TGT_AUTOCORR_FLAG);
        
    Type *fdemoverfs;
    

	if (mxGetClassID(IQ_REF) == mxGetClassID(FDEMOVERFS))
            fdemoverfs = (Type *)mxGetData(FDEMOVERFS);
     else{
           fsType *fdemoverfs_original;
          Type *fdemoverfs_promoted;
          fdemoverfs_original = (fsType*)mxGetData(FDEMOVERFS);
	     mxArray *FDEMOVERFS_PROMOTED = mxCreateNumericArray(mxGetNumberOfDimensions(FDEMOVERFS),mxGetDimensions(FDEMOVERFS),mxGetClassID(IQ_REF),mxREAL);
           fdemoverfs_promoted = (Type*)mxGetData(FDEMOVERFS_PROMOTED);		
	     for (idx = 0; idx < mxGetNumberOfElements(FDEMOVERFS); idx++)
           fdemoverfs_promoted[idx] = (Type)fdemoverfs_original[idx];
		fdemoverfs = (Type*)mxGetData(FDEMOVERFS_PROMOTED);
		mxDestroyArray(FDEMOVERFS_PROMOTED);
	}
	
   	mwSize *outsz = (mwSize*)malloc(sizeof(mwSize)*ndim_tgt);
	for(idx=(0);idx<(ndim_tgt);idx++)
		outsz[idx] = size_tgt[idx];
	const mwSize numel_aline = (mwSize)outsz[0];
    outsz[0] = outsz[0]/decimationfactor;
	
    /*Get Input data and Set up Output*/
    const int ksz_samp		= (int)mxGetScalar(KSZ_SAMP);

    Type *ref_i, *ref_q, *tgt_i, *tgt_q, *phi;
    PHI_OUT = mxCreateNumericArray(mxGetNumberOfDimensions(IQ_TGT), outsz, mxGetClassID(IQ_TGT), mxREAL);
    phi = (Type *) mxGetData(PHI_OUT);
    ref_i = (Type *) mxGetData(IQ_REF);
    ref_q = (Type *) mxGetImagData(IQ_REF);
    tgt_i = (Type *) mxGetData(IQ_TGT);
    tgt_q = (Type *) mxGetImagData(IQ_TGT);
    loupas_engine(phi,ref_i,ref_q,tgt_i,tgt_q,fdemoverfs,(long)numel_ref,(long)numel_tgt,(long)numel_aline,(long)numel_fdem,ksz_samp,decimationfactor,tgt_autocorr_flag);
   
 }
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
    /* Check Input arguments */
	if (nrhs < 4) {
        mexErrMsgTxt("Not enough input arguments.");
    }
	if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS && mxGetClassID(prhs[1]) == mxDOUBLE_CLASS) {
        if (mxGetClassID(FDEMOVERFS) == mxSINGLE_CLASS)
        loupas<double, float >(nlhs,plhs,nrhs,prhs);
        else
        loupas<double, double >(nlhs,plhs,nrhs,prhs);
	}
    else if (mxGetClassID(prhs[0]) == mxSINGLE_CLASS && mxGetClassID(prhs[1]) == mxSINGLE_CLASS) { 
        if (mxGetClassID(FDEMOVERFS) == mxSINGLE_CLASS)
        loupas<float, float >(nlhs,plhs,nrhs,prhs);
        else
        loupas<float, double >(nlhs,plhs,nrhs,prhs);
	}
	else
	mexErrMsgTxt("IQ_REF and IQ_TGT must be the same Type, and both single or double.");
	
	return;

}
