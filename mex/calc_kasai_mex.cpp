#include "mex.h"
#include "matrix.h"
#include <math.h>
#include "../cpp/kasai_engine.cpp"

/* Input arguments */
#define IQ_REF             prhs[0]
#define IQ_TGT             prhs[1]
#define KSZ_SAMP           prhs[2]
#define DECIMATIONFACTOR   prhs[3]

/* Output arguments */
#define PHI_OUT            plhs[0]

/**
 * Defining the following as a template since input/output data can either be float or double.  This was originally
 * done using a code include for a for loop that was type-agnostic, but didn't conform to C++ standards (and was a bit
 * tricky to follow).
 */


template<typename Type>
void kasai(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {


    int idx;
    const mwSize numel_tgt = mxGetNumberOfElements(IQ_TGT);
    const mwSize numel_ref = mxGetNumberOfElements(IQ_REF);
	const mwSize ndim_tgt = mxGetNumberOfDimensions(IQ_TGT);
	const mwSize* size_tgt = mxGetDimensions(IQ_TGT);

    int decimationfactor;

    if (nrhs == 3) {
        decimationfactor = 1;
    }
    else {
        decimationfactor = (int) mxGetScalar(DECIMATIONFACTOR);
    }
    
	mwSize *outsz = (mwSize*)malloc(sizeof(mwSize)*ndim_tgt);
	for(idx=(0);idx<(ndim_tgt);idx++)
		outsz[idx] = size_tgt[idx];
	const mwSize numel_aline = (mwSize)outsz[0];
    outsz[0] = outsz[0]/decimationfactor;
	
    /*Get Input data and Set up Output*/
    const int ksz_samp		= (int)mxGetScalar(KSZ_SAMP);

    Type *ref_i, *ref_q, *tgt_i, *tgt_q, *phi;
    PHI_OUT = mxCreateNumericArray(mxGetNumberOfDimensions(IQ_TGT), outsz, mxGetClassID(IQ_TGT), mxREAL);
    phi = (Type *) mxGetData(PHI_OUT);
    ref_i = (Type *) mxGetData(IQ_REF);
    ref_q = (Type *) mxGetImagData(IQ_REF);
    tgt_i = (Type *) mxGetData(IQ_TGT);
    tgt_q = (Type *) mxGetImagData(IQ_TGT);
    
 
    kasai_engine(phi,ref_i,ref_q,tgt_i,tgt_q,(long)numel_ref,(long)numel_tgt,(long)numel_aline,ksz_samp,decimationfactor);
    
        
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
    /* Check Input arguments */
	if (nrhs < 3) {
        mexErrMsgTxt("Not enough input arguments.");
    }
	if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS && mxGetClassID(prhs[1]) == mxDOUBLE_CLASS) {
	 kasai<double >(nlhs,plhs,nrhs,prhs);
	}
    else if (mxGetClassID(prhs[0]) == mxSINGLE_CLASS && mxGetClassID(prhs[1]) == mxSINGLE_CLASS) { 
	 kasai<float >(nlhs,plhs,nrhs,prhs);
	}
	else
	mexErrMsgTxt("IQ_REF and IQ_TGT must be the same type, and both single or double.");
	
	return;

}
