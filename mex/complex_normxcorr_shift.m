function cc = complex_normxcorr_shift(iq0,iq1,ksz,phi);
kshft = -floor(ksz/2):floor(ksz/2);
N = numel(iq0);
n1 = size(iq0,1);
cc = zeros(size(iq0));
if numel(phi) == 1
    phi = repmat(phi,size(iq0));
end
for i = 0:N-1
    idx = mod(floor(i/n1)*(n1)+mod((i+kshft),n1),N)+1;
    x0 = iq0(idx);
    x1 = iq1(idx);
    x1 = ifft(fft(iq1(idx)*exp(-1j*pi*phi(i+1))));
    cc(i+1) = mean(conj((x0-mean(x0))/std(x0,1)).*((x1-mean(x1))/std(x1,1)));
end
