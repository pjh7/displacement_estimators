#include "mex.h"
#include "matrix.h"
#include <math.h>
#include <complex>

/* Input arguments */
#define RF_REF          prhs[0]
#define RF_TGT          prhs[1]
#define KSZ_SAMP    prhs[2]
#define DECIMATIONFACTOR prhs[3]

/* Output arguments */
#define CC_OUT        plhs[0]

// Define constants
#define PI 3.14159265

using namespace std;

template<typename Type>
void normxcorrcoef(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    /* Check number of input arguments */
    if (nrhs < 3)
        mexErrMsgTxt("Not enough input arguments.");
		
    /* Declare indexing variables */
    long int idx,idxtgt,outidx,minidx,sidx,kidxr,kidxt,si,ki;
	
	/* Get input/output matrix sizes */
	long int decimationfactor,insz0;
    //mwSize *outsz;
	long int ksz_samp	= (long int)mxGetScalar(KSZ_SAMP);
    
    const mwSize *sz_ref = (mwSize*)mxGetDimensions(RF_REF);
    const mwSize ndim_ref = mxGetNumberOfDimensions(RF_REF);
    const size_t numel_ref = mxGetNumberOfElements(RF_REF);
    const mxClassID class_ref = mxGetClassID(RF_REF);
    const bool complex_ref = mxIsComplex(RF_REF);
    
    const mwSize *sz_tgt = (mwSize*)mxGetDimensions(RF_TGT);
    const mwSize ndim_tgt = mxGetNumberOfDimensions(RF_TGT);
    const size_t numel_tgt = mxGetNumberOfElements(RF_TGT);
    const mxClassID class_tgt = mxGetClassID(RF_TGT);
    const bool complex_tgt = mxIsComplex(RF_TGT);

    complex<Type> tmp;
    
    if (nrhs==4)
        decimationfactor = 1;
    else
        decimationfactor = (int)mxGetScalar(DECIMATIONFACTOR);
    //outsz = sz_tgt;
    //insz0 = outsz[0];
    //outsz[0] = outsz[0]/decimationfactor;
    
    mwSize *outsz = (mwSize*)malloc(sizeof(mwSize)*ndim_tgt);
	for(idx=(0);idx<(ndim_tgt);idx++)
		outsz[idx] = sz_tgt[idx];
    outsz[0] = outsz[0]/decimationfactor;
    
	/* Check kernel and search sizes against allocated buffers */
	const long int max_kernel = 1024;
    const long int max_search = 1;
	if (ksz_samp>max_kernel){
        mexPrintf("Error: Requested kernel size is set to %d samples. Maximum kernel size is %d samples.\n",ksz_samp,max_kernel);
        mexErrMsgTxt("Maximum kernel size exceeded.");
    }
		/* Declare I/O pointers */
		complex<Type*> ref;
        complex<Type*> tgt;
        complex<Type*> cc_out;
		/* Declare variables */
		complex<Type> mean_ref,sumSq_ref,mean_tgt,sumSq_tgt,mindot,ssa,ssb,ssc,ssd;
		complex<Type> ssx[3];
		complex<Type> ssy[3];
		complex<Type> dot_prod;
		/* Declare kernel buffer */
		complex<Type> Kref[max_kernel];
		complex<Type> Ktgt[max_kernel];
		/* Declare search region buffer */
		complex<Type> SrchWin_ref[max_kernel];
		complex<Type> SrchWin_tgt[max_kernel];
		CC_OUT = mxCreateNumericArray(ndim_tgt,outsz,class_tgt,(complex_ref || complex_tgt) ? mxCOMPLEX : mxREAL);
        cc_out.real()  = (Type*)mxGetData(CC_OUT);
        
        if (complex_ref || complex_tgt){
            cc_out.imag() = (Type*)mxGetImagData(CC_OUT);
        }
        else{
            mxArray *CC_TMP = mxCreateNumericArray(ndim_tgt,sz_tgt,class_tgt,mxREAL);
            cc_out.imag() = (Type*)mxGetData(CC_TMP);
        }
		
        ref.real()       = (Type*)mxGetData(RF_REF);
        if (!complex_ref){
            mxArray *REF_TMP = mxCreateNumericArray(ndim_ref,sz_ref,class_ref,mxREAL);
            ref.imag() = (Type*)mxGetData(REF_TMP);
        }
        else{
            ref.imag()      = (Type*)mxGetImagData(RF_REF);
        }
        
        tgt.real()       = (Type*)mxGetData(RF_TGT);
        if (!complex_tgt){
            mxArray *TGT_TMP = mxCreateNumericArray(ndim_tgt,sz_tgt,class_tgt,mxREAL);
            tgt.imag() = (Type*)mxGetData(TGT_TMP);
        }
        else{
            tgt.imag()      = (Type*)mxGetImagData(RF_TGT);
        }
		
			/* This code snippet is the functional for-loop for calc_normxcorr.cpp. 
	It lives in a separate file so that it is read twice at compilation via an include
	statement, allowing it to run for both single and double inputs.
	*/
	
	/* Loop through target matrix */
    for(idxtgt=(0);idxtgt<(numel_tgt);idxtgt++)
    {
	//mexPrintf("idxtgt = %d, ",idxtgt);
	    if(idxtgt==0){
			/* Initialize running averages */
			mean_ref = 0;
            mean_tgt = 0;
			sidx = idxtgt;
		//	mexPrintf("[");
			/* Accumulate first running average*/
			for (ki = 0;ki<ksz_samp;ki++){
				kidxr = ((idxtgt - (ksz_samp/2) + ki + numel_ref) % numel_ref);
				kidxt = ((idxtgt - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
                tmp.real() = ref.real()[kidxr];
                tmp.imag() = ref.imag()[kidxr];
				mean_ref += tmp/(complex<Type>)ksz_samp;
                mean_tgt += tgt[kidxt]/ksz_samp;
			//	mexPrintf("%g ",(double)ref[kidxt]);
			}
			//mexPrintf("] mu0 = %g mu1 = %g ",(double)mean_ref,(double)mean_tgt);
			sumSq_ref = 0;
			sumSq_tgt = 0;
			//mexPrintf("[");
			for (ki = 0;ki<ksz_samp;ki++){	
				kidxr = ((idxtgt - (ksz_samp/2) + ki + numel_ref) % numel_ref);
				kidxt = ((idxtgt - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
				Kref[ki] = ref[kidxr]-mean_ref;
				Ktgt[ki] = tgt[kidxt]-mean_tgt;
				sumSq_ref += Kref[ki]*Kref[ki];
				sumSq_tgt += Ktgt[ki]*Ktgt[ki];
			//	mexPrintf("%g ",(double)Kref[ki]);
			}
			//mexPrintf("] sigt = %g ",(double)sumSq_tgt);
			//mexPrintf("[");
			for (ki = 0;ki<ksz_samp;ki++){
			/* Normalize by L2-Norm and store in circular buffer */
				SrchWin_ref[ki] = Kref[ki]/sqrt(sumSq_ref/ksz_samp);
				SrchWin_tgt[ki] = Ktgt[ki]/sqrt(sumSq_tgt/ksz_samp);
			//	mexPrintf("%g ",(double)SrchWin_tgt[ki]);
			}
			//mexPrintf("]");
        }
        else {
			/* Increment running average */
			sidx = idxtgt-1;
			ki = -1;
            kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
			kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
			mean_ref -= (ref[kidxr]/ksz_samp);
            mean_tgt -= (tgt[kidxt]/ksz_samp);
            ki = ksz_samp-1;
            kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
			kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
			mean_ref += (ref[kidxr]/ksz_samp);
            mean_tgt += (tgt[kidxt]/ksz_samp);
            /* Build new zero-mean kernel */
			sumSq_ref = 0;
            sumSq_tgt = 0;
            for (ki = 0;ki<ksz_samp;ki++){
                kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
				kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
				Kref[ki] = ref[kidxr]-mean_ref;
                Ktgt[ki] = tgt[kidxt]-mean_tgt;
                sumSq_ref += Kref[ki]*Kref[ki];
                sumSq_tgt += Ktgt[ki]*Ktgt[ki];
            }
			/* Normalize by L2-Norm and store in circular buffer*/
            for (ki = 0;ki<ksz_samp;ki++){
            SrchWin_ref[ki] = Kref[ki]/sqrt(sumSq_ref/ksz_samp);
            SrchWin_tgt[ki] = Ktgt[ki]/sqrt(sumSq_tgt/ksz_samp);
            }
        }
		if (((idxtgt % insz0) % decimationfactor)==0) {
			outidx = (idxtgt % insz0)/decimationfactor + outsz[0]*(idxtgt/insz0);
			//mexPrintf("outidx = %d ",outidx);
			dot_prod = 0;
			for (ki = 0;ki<ksz_samp;ki++){
				//mexPrintf("%g x %g,",(double)SrchWin_ref[ki],(double)SrchWin_tgt[ki]);
				dot_prod+= SrchWin_ref[ki]*SrchWin_tgt[ki];
			}
			cc_out.real()[outidx] = dot_prod.real()/ksz_samp;
            cc_out.imag()[outidx] = dot_prod.imag()/ksz_samp;
        }
		//mexPrintf(" \n");
    }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
    /* Check Input arguments */
	if (nrhs < 4) {
        mexErrMsgTxt("Not enough input arguments.");
    }
	if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS && mxGetClassID(prhs[1]) == mxDOUBLE_CLASS) {
	 normxcorrcoef<double >(nlhs,plhs,nrhs,prhs);
	}
    else if (mxGetClassID(prhs[0]) == mxSINGLE_CLASS && mxGetClassID(prhs[1]) == mxSINGLE_CLASS) { 
	 normxcorrcoef<float >(nlhs,plhs,nrhs,prhs);
	}
	else
		mexErrMsgTxt("RF_REF and RF_TGT must be the same type, and both single or double.");	
	return;
}