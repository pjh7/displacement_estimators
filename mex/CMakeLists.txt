cmake_minimum_required(VERSION 3.2)
project(cpp)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES
    calc_kasai.cpp
    calc_loupas.cpp
    calc_normxcorr.cpp
    omp.h)

add_executable(cpp ${SOURCE_FILES})