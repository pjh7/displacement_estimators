/*
CUDA-Accelerated Normalized Cross Correlation Mex Function
 */

#include "mex.h"
#include "gpu/mxGPUArray.h"
#include <ctime>

/* Input arguments */
#define RF_REF          prhs[0]
#define RF_TGT          prhs[1]
#define KSZ_SAMP    prhs[2]
#define SRCH_SAMP prhs[3]
#define DECIMATIONFACTOR prhs[4]

/* Output arguments */
#define SHIFT_OUT        plhs[0]
#define CC_OUT			plhs[1]

#define MAX_KERNEL 256
#define MAX_WINDOW 128

/*
 * Device code
 */
void __global__ NormXCorr(float const * const RF_ref,
						 float const * const RF_tgt,
						 float * const shift,
						 float * const cc_out,
						 int const ksz_samp,
						 int const srch_samp,
						 int const decimationfactor,
						 long int const numel_ref,
						 long int const numel_tgt,
						 long int const numel_out,
                         int const insz0
						 )
{
    /* Calculate the global linear index, assuming RF_ref 1-d grid. */
	//clock_t startTime, segmentTime;
	long int const outsz0 = insz0/decimationfactor;
    long int outidx = blockDim.x * blockIdx.x + threadIdx.x;
	long int tgtidx = (outidx/outsz0)*insz0 + (outidx % outsz0)*decimationfactor;
	long int refidx = tgtidx % numel_ref;
	if (outidx < numel_out){
	/*	shift[outidx] = (float)tgtidx;
		cc_out[outidx] = (float)refidx;
		}
	if (false){
	*/
	long int minidx,sidx,kidxr,kidxt,si,ki;
	float sum_ref,sumSq_ref,sum_tgt,sumSq_tgt,mindot,ssa,ssb,ssc,ssd;
	float ssx[3];
	float ssy[3];
	float dot_prod[MAX_WINDOW];
	/* Declare kernel buffer */
	float Kref[MAX_KERNEL];
	float Ktgt[MAX_KERNEL];
	/* Declare search region buffer */
	float SrchWin_ref[MAX_KERNEL][MAX_WINDOW];
	float SrchWin_tgt[MAX_KERNEL][MAX_WINDOW];
	long int const ksz_over_2 = (long int)(ksz_samp/2);
	long int const srch_over_2 = (long int)(srch_samp/2);
	//long int offset;
	/* Loop through target matrix */
    //startTime = clock();
			/* Initialize running averages */
			sum_ref = 0;
            sum_tgt = 0;
            for (si=0;si<srch_samp;si++){
                sidx = si - (srch_over_2);
                if (si == 0){
					/* Accumulate first running average*/
                    for (ki = 0;ki<ksz_samp;ki++){
                        kidxr = refidx+sidx+ki-ksz_over_2;
						kidxt = tgtidx+sidx+ki-ksz_over_2;
						if (kidxr >= numel_ref)
							kidxr -= numel_ref;
						if (kidxr < 0)
							kidxr += numel_ref;
						if (kidxt >= numel_tgt)
							kidxt -= numel_tgt;
						if (kidxt <0)
							kidxt += numel_tgt;
					    sum_ref += RF_ref[kidxr];
                        sum_tgt += RF_tgt[kidxt];
                    }
                }
                else {
					/* Increment running average */
                    ki = -1;
                    kidxr = refidx+sidx+ki-ksz_over_2;
					kidxt = tgtidx+sidx+ki-ksz_over_2;
					if (kidxr >= numel_ref)
						kidxr -= numel_ref;
					if (kidxr < 0)
						kidxr += numel_ref;
					if (kidxt >= numel_tgt)
						kidxt -= numel_tgt;
					if (kidxt <0)
						kidxt += numel_tgt;
					sum_ref -= (RF_ref[kidxr]);
					sum_tgt -= (RF_tgt[kidxt]);
                    ki = ksz_samp-1;
                    kidxr = refidx+sidx+ki-ksz_over_2;
					kidxt = tgtidx+sidx+ki-ksz_over_2;
					if (kidxr >= numel_ref)
						kidxr -= numel_ref;
					if (kidxr < 0)
						kidxr += numel_ref;
					if (kidxt >= numel_tgt)
						kidxt -= numel_tgt;
					if (kidxt <0)
						kidxt += numel_tgt;
					sum_ref += (RF_ref[kidxr]);
                    sum_tgt += (RF_tgt[kidxt]);
				}
				/* Build new zero-mean kernel */ 
                sumSq_ref = 0;
                sumSq_tgt = 0;
                for (ki = 0;ki<ksz_samp;ki++){
                    kidxr = refidx+sidx+ki-ksz_over_2;
					kidxt = tgtidx+sidx+ki-ksz_over_2;
					if (kidxr >= numel_ref)
						kidxr -= numel_ref;
					if (kidxr < 0)
						kidxr += numel_ref;
					if (kidxt >= numel_tgt)
						kidxt -= numel_tgt;
					if (kidxt <0)
						kidxt += numel_tgt;
					Kref[ki] = RF_ref[kidxr]-(sum_ref/ksz_samp);
                    Ktgt[ki] = RF_tgt[kidxt]-(sum_tgt/ksz_samp);
                    sumSq_ref += Kref[ki]*Kref[ki];
                    sumSq_tgt += Ktgt[ki]*Ktgt[ki];
                }
				/* Normalize by L2-Norm and store in circular buffer */
                for (ki = 0;ki<ksz_samp;ki++){
                    SrchWin_ref[ki][si] = Kref[ki]/sqrtf(sumSq_ref);
                    SrchWin_tgt[ki][si] = Ktgt[ki]/sqrtf(sumSq_tgt);
                }
            }
            /* Calculate correlation coefficients (inner products) */
			mindot = -1;
			minidx = 0;
            for (si=0;si<srch_samp;si++){
				dot_prod[si] = 0;
				for (ki = 0;ki<ksz_samp;ki++){
					dot_prod[si]+= (SrchWin_ref[ki][si])*(SrchWin_tgt[ki][(srch_samp-1)-si]);
                }
                if (dot_prod[si]>mindot) {
                    mindot = dot_prod[si];
                    minidx = si;
                }
            }
            
			if (minidx==0 || minidx==(srch_samp-1))
            {
                shift[outidx] = (float)(2*(minidx)-(srch_samp-1));
                cc_out[outidx] = (float)mindot;
            }
            else{
				// quadratic subsample peak estimation //
				ssx[0] = (float)(2*(minidx-1) - (srch_samp-1));
                ssx[1] = (float)(2*(minidx) - (srch_samp-1));
                ssx[2] = (float)(2*(minidx+1) - (srch_samp-1));
                ssy[0] = (float)dot_prod[minidx-1];
                ssy[1] = (float)dot_prod[minidx];
                ssy[2] = (float)dot_prod[minidx+1];
                ssd = (ssx[0] - ssx[1]) * (ssx[0] - ssx[2]) * (ssx[1] - ssx[2]);
                ssa = (ssx[2] * (ssy[1] - ssy[0]) + ssx[1] * (ssy[0] - ssy[2]) + ssx[0] * (ssy[2] - ssy[1])) / ssd;
                ssb = (ssx[2]*ssx[2] * (ssy[0] - ssy[1]) + ssx[1]*ssx[1] * (ssy[2] - ssy[0]) + ssx[0]*ssx[0] * (ssy[1] - ssy[2])) / ssd;
                ssc = (ssx[1] * ssx[2] * (ssx[1] - ssx[2]) * ssy[0] + ssx[2] * ssx[0] * (ssx[2] - ssx[0]) * ssy[1] + ssx[0] * ssx[1] * (ssx[0] - ssx[1]) * ssy[2]) / ssd;
                shift[outidx] = (float)((ssb/(2*ssa)));
				cc_out[outidx] = (float)(ssc-((ssb*ssb)/(4*ssa)));
				//shift[outidx] = (float)threadIdx.x;
				//cc_out[outidx] = (float)(segmentTime);
            }
			
			//cc_out[outidx] =  1000000*round(abs(dot_prod[0])*10)+10000*round(abs(dot_prod[1])*10)+100*round(abs(dot_prod[2])*10)+round(abs(dot_prod[3])*10);//dot_prod[5];//100*(float)minidx + (float)(srch_over_2);
			//shift[outidx] = minidx;
    }
}
/*
 * Host code
 */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, mxArray const *prhs[])
{
	/* Declare all variables.*/
    mxGPUArray const *RF_ref;
	mxGPUArray const *RF_tgt;
    mxGPUArray *shift;
	mxGPUArray *cc_out;
    float const *d_RF_ref;
	float const *d_RF_tgt;
    float *d_shift_out;
	float *d_cc_out;
    //int N;
	//int ndim;
	//mwSize const *sz;
	//int ii;


    /* Choose RF_ref reasonably sized number of threads for the block. */
    int const threadsPerBlock = 1024;
    int blocksPerGrid;

	if (nrhs < 4)
        mexErrMsgTxt("Not enough input arguments.");
	if (nlhs > 2)
		mexErrMsgTxt("Too many output arguments.");
		
    /* Initialize the MathWorks GPU API. */
    mxInitGPU();

    RF_ref = mxGPUCreateFromMxArray(RF_REF);
	RF_tgt = mxGPUCreateFromMxArray(RF_TGT);

    /*
     * Verify that RF_ref really is RF_ref single array before extracting the pointer.
     */
    if (mxGPUGetClassID(RF_ref) != mxSINGLE_CLASS || mxGPUGetClassID(RF_tgt) != mxSINGLE_CLASS) {
        mexErrMsgTxt("RF_ref and RF_tgt must be single");
    }

		
	/* Declare indexing variables */
    //long int tgtidx,outidx,minidx,sidx,kidxr,kidxt,si,ki;
	
	/* Get input/output matrix sizes */
	long int decimationfactor,insz0;
     mwSize *outsz;
	long int ksz_samp	= (long int)mxGetScalar(KSZ_SAMP);
    long int srch_samp	= (long int)mxGetScalar(SRCH_SAMP);
	srch_samp = 2*(srch_samp/2)+1;
    size_t numel_tgt = (long int)mxGPUGetNumberOfElements(RF_tgt);
    size_t numel_ref = (long int)mxGPUGetNumberOfElements(RF_ref);
    if (nrhs==4)
        decimationfactor = 1;
    else
        decimationfactor = (int)mxGetScalar(DECIMATIONFACTOR);
    outsz = (mwSize*)mxGPUGetDimensions(RF_tgt);
    insz0 = outsz[0];
    outsz[0] = outsz[0]/decimationfactor;
    
	/* Check kernel and search sizes against allocated buffers */

	if (ksz_samp>MAX_KERNEL){
        mexPrintf("Error: Requested kernel size is set to %d samples. Maximum kernel size is %d samples.\n",ksz_samp,MAX_KERNEL);
        mexErrMsgTxt("Maximum kernel size exceeded.");
    }
	
    if (srch_samp>MAX_WINDOW){
        mexPrintf("Error: Requested search window is set to %d samples. Maximum search window is %d samples.",srch_samp,MAX_WINDOW);
        mexErrMsgTxt("Maximum search window size exceeded.");
    }
	
	
    /*
     * Now that we have verified the data type, extract RF_ref pointer to the input
     * data on the device.
     */
    d_RF_ref = (float const *)(mxGPUGetDataReadOnly(RF_ref));
	d_RF_tgt = (float const *)(mxGPUGetDataReadOnly(RF_tgt));
	

    /* Create shift GPUArray to hold the result and get its underlying pointer. */
    shift = mxGPUCreateGPUArray(mxGPUGetNumberOfDimensions(RF_tgt),
                            outsz,
                            mxGPUGetClassID(RF_tgt),
                            mxGPUGetComplexity(RF_tgt),
                            MX_GPU_DO_NOT_INITIALIZE);
    d_shift_out = (float *)(mxGPUGetData(shift));
	size_t numel_out = (long int)mxGPUGetNumberOfElements(shift);
	cc_out = mxGPUCreateGPUArray(mxGPUGetNumberOfDimensions(RF_tgt),
                            outsz,
                            mxGPUGetClassID(RF_tgt),
                            mxGPUGetComplexity(RF_tgt),
                            MX_GPU_DO_NOT_INITIALIZE);
    d_cc_out = (float *)(mxGPUGetData(cc_out));
	blocksPerGrid = (numel_out + threadsPerBlock - 1) / threadsPerBlock;
    
    /*
     * Call the kernel using the CUDA runtime API. We are using RF_ref 1-d grid here,
     * and it would be possible for the number of elements to be too large for
     * the grid. For this example we are not guarding against this possibility.
     */
	/*int const ndim_tgt = (int)(mxGPUGetNumberOfDimensions(RF_tgt));
	if (ndim_tgt<2)
		mexErrMsgIdAndTxt(errId,"Not enough dimensions");
		mexPrintf("sz = [ ");
	for (int ii=0;ii<ndim_tgt;ii++){
	 mexPrintf("%d ",(int)outsz[ii]);
	}
	mexPrintf("];\n");
	*/
	
	//mexPrintf("numel_tgt = %d;\nblocksPerGrid = %d;\nthreadsPerBlock = %d;\n",numel_tgt,blocksPerGrid,threadsPerBlock);
    NormXCorr<<<blocksPerGrid, threadsPerBlock>>>(d_RF_ref,d_RF_tgt,d_shift_out,d_cc_out,ksz_samp,srch_samp,decimationfactor,numel_ref,numel_tgt,numel_out,insz0);

    /* Wrap the result up as RF_ref MATLAB gpuArray for return. */
    plhs[0] = mxGPUCreateMxArrayOnGPU(shift);
	if (nlhs>1)
		plhs[1] = mxGPUCreateMxArrayOnGPU(cc_out);
	
	

    /*
     * The mxGPUArray pointers are host-side structures that refer to device
     * data. These must be destroyed before leaving the MEX function.
     */
    mxGPUDestroyGPUArray(RF_ref);
	mxGPUDestroyGPUArray(RF_tgt);
    mxGPUDestroyGPUArray(shift);
	mxGPUDestroyGPUArray(cc_out);
	return;
	
}
