%CALC_KASAI unitless Kasai's estimator (MEX)
%[phi] = CALC_KASAI(iq_ref,iq_tgt,ksz_samp,[decimationfactor]);
%     
% CALC_Kasai is a mex-ified implementation (c++) of Kasai's 1-D
% autocorrelation algorithm for the estimation of motion in ultrasound
% data. It strings out the data into 1-D vectors for rapid computation,
% which applies a quasi-circular boundary condition. You can remove these
% effects by throwing out the top and bottom half-kernels of data after
% processing.
%-------------------------------------------------------------------------
% INPUTS
%   iq_ref: N-D matrix containing the reference IQ data. May be single
%       or double precision, but must match iq_tgt. A single frame can be
%       used to match multiple iq_tgt frames, but the dimension in which
%       the reference data will be repeated must be the last dimension.
%       Example: iq_ref can be [512x64x1] and iq_tgt can be [512x64x30],
%       but [512x64x1x32] and [512x64x30x32] won't work (permute to
%       [512x64x32x1] and [512x64x32x30] for intended operation.
%
%   iq_tgt: N-D matrix containing the target IQ data. May be single or
%       double precision, but must match iq_ref. 
%
%   ksz_samp: integer axial kernel size. round(ksz_lambda*fs/f0) is a good
%       way to calculate it.
%
%   decimationfactor: integer downscaling factor for output. The algorithm
%       will still calculate every point (because of the way the loop
%       works), but only assigns every decimationfactor sample to the
%       output matrix. Defaults to 1 if not included.
%-------------------------------------------------------------------------
% OUTPUTS
%   phi: calculated phase shift between iq_ref and iq_tgt (radians). Same
%       size as iq_tgt. To get to displacements in meters, use the
%       following: u[m] = phi[rad] / (2*pi) * (c[m/s]/2) * (1/f0[Hz])
%-------------------------------------------------------------------------

% Version 0.1
%
% 2015/07/01 Peter Hollender
%      Mexified code.
%
% In order to compile this code, use the following command:
%
% mex -v calc_kasai.cpp
function [phi] = calc_kasai(varargin)
if exist('calc_kasai_mex')==3
    [phi] = calc_kasai_mex(varargin{:});
else
    [pth,me,ext] = fileparts(mfilename);
    warning('path:nomex','mex file %s_mex.%s not found. Using %s_matlab.m. Consider compiling ../mex/%s_mex.cpp',me,mexext,me,me);
    [phi] = calc_kasai_matlab(varargin{:});
end
