import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sp
import calc_kasai as est
import time
#%%
filename = 'F:/git/oscilloscope/testscripts/push.bin'
nlines = 100
numel_aline = 1001
decimationfactor = 3
rf = np.fromfile(filename,np.int8,numel_aline*nlines)
rf = rf.reshape([nlines,numel_aline],order='C')
ref_iq = np.array([sp.hilbert(rf[0]-np.mean(rf[0]))]).transpose()
tgt_iq = np.array([sp.hilbert(rf[i]-np.mean(rf[i])) for i in range(nlines)]).transpose()
ref_i = ref_iq.real.copy(order = 'F')
ref_q = ref_iq.imag.copy(order = 'F')
tgt_i = tgt_iq.real.copy(order = 'F')
tgt_q = tgt_iq.imag.copy(order = 'F')
tgtsz = tgt_iq.shape
#numel_aline = int(tgtsz[tgt_iq.ndim-1])
outsz = [int(tgtsz[i]) for i in range(tgt_iq.ndim)]
outsz[0] = int(outsz[0]/decimationfactor)
phi = np.empty(outsz,dtype=float,order = 'F')
est.calc_kasai(phi,ref_i,ref_q,tgt_i,tgt_q,3,decimationfactor)
outsz2 = outsz
outsz[0]-=1
time.sleep(2)
dphi = np.array([[phi[i][j+1]-phi[i][j] for j in range(nlines-1)] for i in range(int(phi.shape[0]))],order='F')
#%%
fig,ax = plt.subplots(1,3,figsize=[15,20],dpi=100)  
#ax[0].plot(rf[0])
#ax[0].plot(np.absolute(ref_iq[0]))
#ax[0].plot(np.absolute(tgt_iq[0]))
#ax[1].plot(phi.transpose())
ax[0].imshow(np.absolute(tgt_iq),extent=[0.0,1.0,0.0,3.0],interpolation="nearest")
ax[1].imshow(phi,extent=[0.0,1.0,0.0,3.0],clim=(-1,1),interpolation="nearest")
ax[2].imshow(dphi,extent=[0.0,1.0,0.0,3.0],clim=(-1,1),interpolation="nearest")
print "done"
