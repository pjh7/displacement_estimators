"""
pydispest.pyx

cython implementations of Kasai, Loupas, and Normxcorr.

"""

import cython

# import both numpy and the Cython declarations for numpy
import numpy as np
cimport numpy as np
from cython.operator cimport dereference as deref, preincrement as inc

# declare the interface to the C code
#cdef extern from "..\cpp\loupas_engine.cpp":
#    void loupas_engine[T] (T *phi, T *ref_i, T *ref_q, T *tgt_i, T* tgt_q,T* fdemoverfs, int numel_ref,int numel_tgt,int numel_aline,int numel_fdem,int ksz_samp,int decimationfactor)
#
#@cython.boundscheck(False)
#@cython.wraparound(False)
#
#def calc_loupas(np.ndarray[double, ndim=2, mode="fortran"] phi not None,
#          np.ndarray[double, ndim=2, mode="fortran"] ref_i not None,
#          np.ndarray[double, ndim=2, mode="fortran"] ref_q not None,
#          np.ndarray[double, ndim=2, mode="fortran"] tgt_i not None,
#          np.ndarray[double, ndim=2, mode="fortran"] tgt_q not None,
#          np.ndarray[double, ndim=2, mode="fortran"] fdemoverfs not None,
#          int Ksz_samp, int Decimationfactor):
#    cdef int numel_ref, numel_tgt, numel_aline, ksz_samp, decimationfactor
#    ksz_samp = Ksz_samp
#    decimationfactor = Decimationfactor
#    numel_ref = ref_i.size
#    numel_tgt = tgt_i.size
#    tgtsz = tgt_i.shape
#    numel_aline = int(tgtsz[0])
#    numel_fdem = fdemoverfs.size
#    #outsz = [int(tgtsz[i]) for i in range(tgt_i.ndim)]
#    #outsz[tgt_i.ndim-1] = outsz[tgt_i.ndim-1]/decimationfactor
#    loupas_engine[double] (&phi[0,0],&ref_i[0,0],&ref_q[0,0],&tgt_i[0,0],&tgt_q[0,0],&fdemoverfs[0,0],numel_ref,numel_tgt,numel_aline,numel_fdem,ksz_samp,decimationfactor)
#    
cdef extern from r"..\cpp\kasai_engine.cpp":
    void kasai_engine[T] (T *phi, T *ref_i, T *ref_q, T *tgt_i, T* tgt_q,int numel_ref,int numel_tgt,int numel_aline,int ksz_samp,int decimationfactor)


def calc_kasai(np.ndarray[double, ndim=2, mode="fortran"] phi not None,
          np.ndarray[double, ndim=2, mode="fortran"] ref_i not None,
          np.ndarray[double, ndim=2, mode="fortran"] ref_q not None,
          np.ndarray[double, ndim=2, mode="fortran"] tgt_i not None,
          np.ndarray[double, ndim=2, mode="fortran"] tgt_q not None,
          int Ksz_samp, int Decimationfactor):
    cdef int numel_ref, numel_tgt, numel_aline, ksz_samp, decimationfactor
    ksz_samp = Ksz_samp
    decimationfactor = Decimationfactor
    numel_ref = ref_i.size
    numel_tgt = tgt_i.size
    tgtsz = tgt_i.shape
    numel_aline = int(tgtsz[0])
    #outsz = [int(tgtsz[i]) for i in range(tgt_i.ndim)]
    #outsz[tgt_i.ndim-1] = outsz[tgt_i.ndim-1]/decimationfactor
    kasai_engine[double] (&phi[0,0],&ref_i[0,0],&ref_q[0,0],&tgt_i[0,0],&tgt_q[0,0],numel_ref,numel_tgt,numel_aline,ksz_samp,decimationfactor)
    

cdef extern from r"..\cpp\normxcorr_engine.cpp":
    void normxcorr_engine[T] (T *shift, T *cc, T *ref, T *tgt,int numel_ref,int numel_tgt,int numel_aline,int ksz_samp,int srch_samp,int calc_cc, int decimationfactor)

#cdef extern from r"..\cpp\normxcorr_engine_double.cpp":
#    void normxcorr_engine_double (double *shift, double *cc, double *ref_rf, double *tgt,int numel_ref,int numel_tgt,int numel_aline,int ksz_samp,int srch_samp,int calc_cc, int decimationfactor)


def calc_normxcorr(np.ndarray[double, ndim=2, mode="fortran"] shift not None,
          np.ndarray[double, ndim=2, mode="fortran"] cc not None,
          np.ndarray[double, ndim=2, mode="fortran"] ref not None,
          np.ndarray[double, ndim=2, mode="fortran"] tgt not None,
          int Ksz_samp, int Srch_samp, int Calc_cc, int Decimationfactor):
    cdef int numel_ref, numel_tgt, numel_aline, ksz_samp, srch_samp, decimationfactor
    cdef int calc_cc  
    ksz_samp = Ksz_samp
    srch_samp = Srch_samp
    calc_cc = Calc_cc
    decimationfactor = Decimationfactor
    numel_ref = ref.size
    numel_tgt = tgt.size
    tgtsz = tgt.shape
    numel_aline = int(tgtsz[0])
    normxcorr_engine[double] (&shift[0,0],&cc[0,0],&ref[0,0],&tgt[0,0],numel_ref,numel_tgt,numel_aline,ksz_samp,srch_samp,calc_cc,decimationfactor)
    #normxcorr_engine_double(&shift[0,0],&cc[0,0],&ref[0,0],&tgt[0,0],numel_ref,numel_tgt,numel_aline,ksz_samp,srch_samp,calc_cc,decimationfactor)

def normxcorr(ref,tgt,ksz_samp,srch_samp,calc_cc=0,downsample=1):
    ref = np.array(ref,copy=False)
    ref = ref.reshape([ref.shape[0],-1]).astype('float', order='F')
    out_shape = list(tgt.shape);
    out_shape[0] = int(out_shape[0]/downsample)
    tgt = np.array(tgt,copy=False)
    tgt = tgt.reshape([tgt.shape[0],-1]).astype('float', order='F')
    shift = np.empty([int(tgt.shape[0]/downsample), tgt.shape[1]], order='F')
    cc = np.empty([int(tgt.shape[0]/downsample), tgt.shape[1]], order='F')
    calc_normxcorr(shift,cc,ref,tgt,ksz_samp,srch_samp,calc_cc,downsample)
    if calc_cc:
        return shift.reshape(out_shape), cc.reshape(out_shape)
    else:
        return shift.reshape(out_shape)