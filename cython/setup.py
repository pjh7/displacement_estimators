from __future__ import print_function
try:
    from setuptools import setup
    from setuptools import Extension
    print("using setuptools")
except ImportError:
    print("could not use setuptools. Using distutils")
    from distutils.core import setup
    from distutils.extension import Extension
    
#from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy
import os
#extensions = [Extension('pydispest',['pydispest.pyx',
#                                     r'../cpp/kasai_engine.cpp',
#                                     r'../cpp/loupas_engine.cpp',
#                                     r'../cpp/normxcorr_engine.cpp'
#                                     ],
#                                    include_dirs = [numpy.get_include(),r'../cpp/'],language = "c++")]
extensions = [Extension('pydispest',['pydispest.pyx',
                                     r'../cpp/kasai_engine.cpp',
                                     r'../cpp/normxcorr_engine.cpp'
                                     ],
                                    include_dirs = [numpy.get_include(),r'../cpp/'],language = "c++")]    
    
setup(
    name = "displacement estimators",
    ext_modules = cythonize(extensions),
)