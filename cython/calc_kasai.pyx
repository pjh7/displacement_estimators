"""
calc_kasai.pyx

cython implementation of Kasai's autocorrelator. 

"""

import cython

# import both numpy and the Cython declarations for numpy
import numpy as np
cimport numpy as np
from cython.operator cimport dereference as deref, preincrement as inc

# declare the interface to the C code
cdef extern from "..\cpp\kasai_engine.cpp":
    void kasai_engine[T] (T *phi, T *ref_i, T *ref_q, T *tgt_i, T* tgt_q,int numel_ref,int numel_tgt,int numel_aline,int ksz_samp,int decimationfactor)

@cython.boundscheck(False)
@cython.wraparound(False)

def calc_kasai(np.ndarray[double, ndim=2, mode="fortran"] phi not None,
          np.ndarray[double, ndim=2, mode="fortran"] ref_i not None,
          np.ndarray[double, ndim=2, mode="fortran"] ref_q not None,
          np.ndarray[double, ndim=2, mode="fortran"] tgt_i not None,
          np.ndarray[double, ndim=2, mode="fortran"] tgt_q not None,
          int Ksz_samp, int Decimationfactor):
    cdef int numel_ref, numel_tgt, numel_aline, ksz_samp, decimationfactor
    ksz_samp = Ksz_samp
    decimationfactor = Decimationfactor
    numel_ref = ref_i.size
    numel_tgt = tgt_i.size
    tgtsz = tgt_i.shape
    numel_aline = int(tgtsz[tgt_i.ndim-1])
    numel_aline = int(tgtsz[0])
    #outsz = [int(tgtsz[i]) for i in range(tgt_i.ndim)]
    #outsz[tgt_i.ndim-1] = outsz[tgt_i.ndim-1]/decimationfactor
    kasai_engine[double] (&phi[0,0],&ref_i[0,0],&ref_q[0,0],&tgt_i[0,0],&tgt_q[0,0],numel_ref,numel_tgt,numel_aline,ksz_samp,decimationfactor)
    

