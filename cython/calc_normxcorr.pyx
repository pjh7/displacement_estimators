"""
calc_kasai.pyx

cython implementation of Kasai's autocorrelator. 

"""

import cython

# import both numpy and the Cython declarations for numpy
import numpy as np
cimport numpy as np
from cython.operator cimport dereference as deref, preincrement as inc

# declare the interface to the C code
cdef extern from "..\cpp\normxcorr_engine.cpp":
    void normxcorr_engine[T] (T *shift, T *cc, T *ref, T *tgt,int numel_ref,int numel_tgt,int numel_aline,int ksz_samp,int srch_samp,bool calc_cc, int decimationfactor)

@cython.boundscheck(False)
@cython.wraparound(False)

def calc_normxcorr(np.ndarray[double, ndim=2, mode="fortran"] shift not None,
          np.ndarray[double, ndim=2, mode="fortran"] cc not None,
          np.ndarray[double, ndim=2, mode="fortran"] ref not None,
          np.ndarray[double, ndim=2, mode="fortran"] tgt not None,
          int Ksz_samp, int Srch_samp, bool Calc_cc, int Decimationfactor):
    cdef int numel_ref, numel_tgt, numel_aline, ksz_samp, srch_samp, decimationfactor
    cdef bool calc_cc    
    ksz_samp = Ksz_samp
    srch_samp = Srch_samp
    calc_cc = Calc_cc
    decimationfactor = Decimationfactor
    numel_ref = ref.size
    numel_tgt = tgt.size
    tgtsz = tgt.shape
    numel_aline = int(tgtsz[tgt.ndim-1])
    numel_aline = int(tgtsz[0])
    normxcorr_engine[double] (&shift[0,0],&cc[0,0],&ref[0,0],&tgt[0,0],numel_ref,numel_tgt,numel_aline,ksz_samp,srch_samp,calc_cc,decimationfactor)
    

