//#include <matrix.h>
//#include <math.h>
#include "loupas_engine.h"

/*
 * loupas_engine is the function c++ code to calculate the phase shift between two sets of ultrasound IQ a-lines. If numel_tgt > numel_ref, a-lines in the reference are re-used in order.
 * :param phi: output array TYPE pointer
 * :param ref_i: In-phase component of reference signal TYPE pointer
 * :param ref_q: Quadrature component of reference signal TYPE pointer
 * :param tgt_i: In-phase component of target signal TYPE pointer
 * :param tgt_q: Quadrature component of target signal TYPE pointer
 * :param fdemoverfs: demodulation frequency, scaled by sampling frequency TYPE pointer
 * :param numel_ref: total number of elements in the reference signal
 * :param numel_tgt: total number of elements in the target signal
 * :param numel_aline: number of elements in each a-line. numel_ref = numel_aline * number of a-alines
 * :param numel_aline: number of elements in fdemoverfs
 * :param ksz_samp: kernel size in samples for averaging the phase
 * :param decimationfactor: downsampling factor - output phase is calculated at every decimationfactor'th element for each a-line
 */

#define PI 3.14159265

template<typename Type>
        void loupas_engine(Type *phi, Type *ref_i, Type *ref_q, Type *tgt_i, Type *tgt_q, Type *fdemoverfs, const long numel_ref, const long numel_tgt, const long numel_aline, const long numel_fdem, const int ksz_samp, const int decimationfactor, const int tgt_autocorr_flag){
    Type localsum0[4] = {0,0,0,0};
    Type localsum1 [4] = {0,0,0,0};
    const int numel_aline_out = numel_aline/decimationfactor;
    long idx, idxref, idxref1, idxtgt, idxtgt1;
    
    /* Loop through array */
    for(idx=(0);idx<(numel_tgt+(ksz_samp));idx++)
    {
        if(idx < (ksz_samp)){
            //The first ksz_samp iterations of the loop build up the convolution kernel buffers, using circular indexing.
            idxref = (idx-ksz_samp/2 + numel_ref) % numel_ref;
            idxtgt = (idx-ksz_samp/2 + numel_tgt) % numel_tgt;
            //
            localsum0[0] += (ref_q[idxref]*tgt_i[idxtgt]);
            localsum0[1] += (ref_i[idxref]*tgt_q[idxtgt]);
            localsum0[2] += (ref_i[idxref]*tgt_i[idxtgt]);
            localsum0[3] += (ref_q[idxref]*tgt_q[idxtgt]);
            if (idx > 0){
                idxref = (idx-ksz_samp/2 - 1 + numel_ref) % numel_ref;
                idxtgt = (idx-ksz_samp/2 - 1 + numel_tgt) % numel_tgt;
                idxref1 = (idx-ksz_samp/2 + numel_ref) % numel_ref;
                if (tgt_autocorr_flag>0){
                    idxtgt1 = (idx-ksz_samp/2 + numel_tgt) % numel_tgt;
                    localsum1[0] += (ref_q[idxref]*ref_i[idxref1] + tgt_q[idxtgt]*tgt_i[idxtgt1]);
                    localsum1[1] += (ref_i[idxref]*ref_q[idxref1] + tgt_i[idxtgt]*tgt_q[idxtgt1]);
                    localsum1[2] += (ref_i[idxref]*ref_i[idxref1] + tgt_i[idxtgt]*tgt_i[idxtgt1]);
                    localsum1[3] += (ref_q[idxref]*ref_q[idxref1] + tgt_q[idxtgt]*tgt_q[idxtgt1]); 
                }
                else{
                    localsum1[0] += (ref_q[idxref]*ref_i[idxref1]);
                    localsum1[1] += (ref_i[idxref]*ref_q[idxref1]);
                    localsum1[2] += (ref_i[idxref]*ref_i[idxref1]);
                    localsum1[3] += (ref_q[idxref]*ref_q[idxref1]);
                }
            }
        }
        else {
            //compute the phase angle for the current buffers, assign to the output
            idxtgt = idx-ksz_samp;
            //mexPrintf("%d: %d\n",idxtgt,(idxtgt % numel_aline)/decimationfactor + outsz[0]*(idxtgt/numel_aline));
            phi[(idxtgt % numel_aline)/decimationfactor + numel_aline_out*(idxtgt/numel_aline)] = (Type)(fdemoverfs[idxtgt % numel_fdem]*atan2(localsum0[0]-localsum0[1],localsum0[2]+localsum0[3])/(fdemoverfs[idxtgt % numel_fdem]+atan2(localsum1[1]-localsum1[0],localsum1[2]+localsum1[3])/(2*PI)));
            //mexPrintf("%g\n",fdemoverfs[idxtgt % numel_fdem]);// subtract the earliest sample from the buffers
            //mexPrintf("%g-%g/%g+%g\n",localsum0[0],localsum0[1],localsum0[2],localsum0[3]);
            idxref = (idx-ksz_samp-ksz_samp/2 + numel_ref) % numel_ref;
            idxtgt = (idx-ksz_samp-ksz_samp/2 + numel_tgt) % numel_tgt;
            //idxtgt1 = (idx-ksz_samp-ksz_samp/2-1 + numel_tgt) % numel_tgt;
            localsum0[0] -= (Type)(ref_q[idxref]*tgt_i[idxtgt]);
            localsum0[1] -= (Type)(ref_i[idxref]*tgt_q[idxtgt]);
            localsum0[2] -= (Type)(ref_i[idxref]*tgt_i[idxtgt]);
            localsum0[3] -= (Type)(ref_q[idxref]*tgt_q[idxtgt]);
            idxref = (idx-ksz_samp-ksz_samp/2 + numel_ref) % numel_ref;
            idxtgt = (idx-ksz_samp-ksz_samp/2 + numel_tgt) % numel_tgt;
            idxref1 = (idx-ksz_samp-ksz_samp/2 + 1 + numel_ref) % numel_ref;
            if (tgt_autocorr_flag>0){
                idxtgt1 = (idx-ksz_samp-ksz_samp/2 + 1 + numel_tgt) % numel_tgt;
                localsum1[0] -= (ref_q[idxref]*ref_i[idxref1] + tgt_q[idxtgt]*tgt_i[idxtgt1]);
                localsum1[1] -= (ref_i[idxref]*ref_q[idxref1] + tgt_i[idxtgt]*tgt_q[idxtgt1]);
                localsum1[2] -= (ref_i[idxref]*ref_i[idxref1] + tgt_i[idxtgt]*tgt_i[idxtgt1]);
                localsum1[3] -= (ref_q[idxref]*ref_q[idxref1] + tgt_q[idxtgt]*tgt_q[idxtgt1]);
            }
            else{
                localsum1[0] -= (ref_q[idxref]*ref_i[idxref1]);
                localsum1[1] -= (ref_i[idxref]*ref_q[idxref1]);
                localsum1[2] -= (ref_i[idxref]*ref_i[idxref1]);
                localsum1[3] -= (ref_q[idxref]*ref_q[idxref1]);
            }
            // add the new sample to the buffers
            idxref = (idx-ksz_samp/2 + numel_ref) % numel_ref;
            idxtgt = (idx-ksz_samp/2 + numel_tgt) % numel_tgt;
            //idxtgt1 = (idx-ksz_samp/2-1 + numel_tgt) % numel_tgt;
            localsum0[0] += (Type)(ref_q[idxref]*tgt_i[idxtgt]);
            localsum0[1] += (Type)(ref_i[idxref]*tgt_q[idxtgt]);
            localsum0[2] += (Type)(ref_i[idxref]*tgt_i[idxtgt]);
            localsum0[3] += (Type)(ref_q[idxref]*tgt_q[idxtgt]);
            idxref = (idx-ksz_samp/2 - 1 + numel_ref) % numel_ref;
            idxtgt = (idx-ksz_samp/2 - 1 + numel_tgt) % numel_tgt;
            idxref1 = (idx-ksz_samp/2 + numel_ref) % numel_ref;
            if (tgt_autocorr_flag>0){
                idxtgt1 = (idx-ksz_samp/2 + numel_tgt) % numel_tgt;
                localsum1[0] += (ref_q[idxref]*ref_i[idxref1] + tgt_q[idxtgt]*tgt_i[idxtgt1]);
                localsum1[1] += (ref_i[idxref]*ref_q[idxref1] + tgt_i[idxtgt]*tgt_q[idxtgt1]);
                localsum1[2] += (ref_i[idxref]*ref_i[idxref1] + tgt_i[idxtgt]*tgt_i[idxtgt1]);
                localsum1[3] += (ref_q[idxref]*ref_q[idxref1] + tgt_q[idxtgt]*tgt_q[idxtgt1]);
            }
            else{
                localsum1[0] += (ref_q[idxref]*ref_i[idxref1]);
                localsum1[1] += (ref_i[idxref]*ref_q[idxref1]);
                localsum1[2] += (ref_i[idxref]*ref_i[idxref1]);
                localsum1[3] += (ref_q[idxref]*ref_q[idxref1]);
            }
            
        }
    }
}