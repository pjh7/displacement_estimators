#include "normxcorr_engine_double.h"
#include <iostream>
using namespace std;
//template<doublename double>
void normxcorr_engine_double(double *shift, double*cc, double *ref_rf, double *tgt, const long numel_ref, const long numel_tgt, const long numel_aline, const long ksz_samp, const long srch_samp, const int calc_cc, const int decimationfactor){
long idx,outidx,minidx,sidx,kidxr,kidxt,si,ki;
const long numel_aline_out = numel_aline/decimationfactor;
double mean_ref,sumSq_ref,mean_tgt,sumSq_tgt,mindot,ssa,ssb,ssc,ssd;
double ssx[3];
double ssy[3];
double dot_prod[MAX_SEARCH];
/* Declare kernel buffer */
double Kref[MAX_KERNEL];
double Ktgt[MAX_KERNEL];
/* Declare search region buffer */
double SrchWin_ref[MAX_KERNEL][MAX_SEARCH];
double SrchWin_tgt[MAX_KERNEL][MAX_SEARCH];
/* Loop through target matrix */
printf("Loading");
    for(idx=(0);idx<(numel_tgt);idx++)
    {
	    if(idx==0){
			/* Initialize running averages */
			mean_ref = 0;
            mean_tgt = 0;
            for (si=0;si<srch_samp;si++){
                sidx = idx - (srch_samp/2) + si;
                if (si == 0){
					/* Accumulate first running average*/
                    for (ki = 0;ki<ksz_samp;ki++){
                        kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
						kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
					    mean_ref += ref_rf[kidxr]/ksz_samp;
                        mean_tgt += tgt[kidxt]/ksz_samp;
                    }
                }
                else {
					/* Increment running average */
                    ki = -1;
                    kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
					kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
					mean_ref -= (ref_rf[kidxr]/ksz_samp);
                    mean_tgt -= (tgt[kidxt]/ksz_samp);
                    ki = ksz_samp-1;
                    kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
					kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
					mean_ref += (ref_rf[kidxr]/ksz_samp);
                    mean_tgt += (tgt[kidxt]/ksz_samp);
				}
				/* Build new zero-mean kernel */ 
                sumSq_ref = 0;
                sumSq_tgt = 0;
                for (ki = 0;ki<ksz_samp;ki++){
                    kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
					kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
					Kref[ki] = ref_rf[kidxr]-mean_ref;
                    Ktgt[ki] = tgt[kidxt]-mean_tgt;
                    sumSq_ref += Kref[ki]*Kref[ki];
                    sumSq_tgt += Ktgt[ki]*Ktgt[ki];
                }
				/* Normalize by L2-Norm and store in circular buffer */
                for (ki = 0;ki<ksz_samp;ki++){
                    SrchWin_ref[ki][si] = Kref[ki];///sqrt(sumSq_ref);
                    SrchWin_tgt[ki][si] = Ktgt[ki];///sqrt(sumSq_tgt);
                }
            }
        }
        else {
			/* Increment running average */
			si = srch_samp-1;
			sidx = idx - (srch_samp/2) + si;
			ki = -1;
            kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
			kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
			mean_ref -= (ref_rf[kidxr]/ksz_samp);
            mean_tgt -= (tgt[kidxt]/ksz_samp);
            ki = ksz_samp-1;
            kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
			kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
			mean_ref += (ref_rf[kidxr]/ksz_samp);
            mean_tgt += (tgt[kidxt]/ksz_samp);
            /* Build new zero-mean kernel */
			sumSq_ref = 0;
            sumSq_tgt = 0;
            for (ki = 0;ki<ksz_samp;ki++){
                kidxr = ((sidx - (ksz_samp/2) + ki + numel_ref) % numel_ref);
				kidxt = ((sidx - (ksz_samp/2) + ki + numel_tgt) % numel_tgt);
				Kref[ki] = ref_rf[kidxr]-mean_ref;
                Ktgt[ki] = tgt[kidxt]-mean_tgt;
                sumSq_ref += Kref[ki]*Kref[ki];
                sumSq_tgt += Ktgt[ki]*Ktgt[ki];
            }
			/* Normalize by L2-Norm and store in circular buffer*/
            for (ki = 0;ki<ksz_samp;ki++){
            SrchWin_ref[ki][(idx - 1) % srch_samp] = Kref[ki];///sqrt(sumSq_ref);
            SrchWin_tgt[ki][(idx - 1) % srch_samp] = Ktgt[ki];///sqrt(sumSq_tgt);
            }
        }
		if (((idx % numel_aline) % decimationfactor)==0) {
			outidx = (idx % numel_aline)/decimationfactor + numel_aline_out*(idx/numel_aline);
            /* Calculate correlation coefficients (innter products) */
            mindot = -1;
            for (si=0;si<srch_samp;si++){
                dot_prod[si] = 0;
                for (ki = 0;ki<ksz_samp;ki++){
                    dot_prod[si]+= SrchWin_ref[ki][(si+idx) % srch_samp]*SrchWin_tgt[ki][(srch_samp-si-1+idx) % srch_samp];
                }
                if (dot_prod[si]>mindot) {
                    mindot = dot_prod[si];
                    minidx = si;
                }
            }
            if (minidx==0 || minidx==(srch_samp-1))
            {
                shift[outidx] = (double)(2*(minidx)-(srch_samp-1));
                if (calc_cc>0){
                    cc[outidx] = mindot;
                    
                }
            }
            else{
				/* quadratic subsample peak estimation */
                ssx[0] = (double)(2*(minidx-1) - (srch_samp-1));
                ssx[1] = (double)(2*(minidx) - (srch_samp-1));
                ssx[2] = (double)(2*(minidx+1) - (srch_samp-1));
                ssy[0] = dot_prod[minidx-1];
                ssy[1] = dot_prod[minidx];
                ssy[2] = dot_prod[minidx+1];
                ssd = (ssx[0] - ssx[1]) * (ssx[0] - ssx[2]) * (ssx[1] - ssx[2]);
                ssa = (ssx[2] * (ssy[1] - ssy[0]) + ssx[1] * (ssy[0] - ssy[2]) + ssx[0] * (ssy[2] - ssy[1])) / ssd;
                ssb = (ssx[2]*ssx[2] * (ssy[0] - ssy[1]) + ssx[1]*ssx[1] * (ssy[2] - ssy[0]) + ssx[0]*ssx[0] * (ssy[1] - ssy[2])) / ssd;
                ssc = (ssx[1] * ssx[2] * (ssx[1] - ssx[2]) * ssy[0] + ssx[2] * ssx[0] * (ssx[2] - ssx[0]) * ssy[1] + ssx[0] * ssx[1] * (ssx[0] - ssx[1]) * ssy[2]) / ssd;
                shift[outidx] = (ssb/(2*ssa));
                if (calc_cc>0)
                {
                    cc[outidx] = ssc-((ssb*ssb)/(4*ssa));
                }
            }
        }
    }
    }