#ifndef kasai_engine_h
#define kasai_engine_h
template<typename Type>
void kasai_engine(Type *phi, Type *ref_i, Type *ref_q, Type *tgt_i, Type* tgt_q, const long numel_ref, const long numel_tgt, const long numel_aline, const int ksz_samp, const int decimationfactor);
#endif