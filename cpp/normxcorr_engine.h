#ifndef normxcorr_engine_h
#define normxcorr_engine_h
//#define MAX_KERNEL 512
//#define MAX_SEARCH 512
template<typename Type>
void normxcorr_engine(Type *shift, Type*cc, Type *ref_rf, Type *tgt_rf, const long numel_ref, const long numel_tgt, const long numel_aline, const long ksz_samp, const long srch_samp, const int calc_cc, const int decimationfactor);
#endif
