//#include <matrix.h>
//#include <math.h>
#include "kasai_engine.h"
/*
kasai_engine is the function c++ code to calculate the phase shift between two sets of ultrasound IQ a-lines. If numel_tgt > numel_ref, a-lines in the reference are re-used in order.
:param phi: output array TYPE pointer
:param ref_i: In-phase component of reference signal TYPE pointer
:param ref_q: Quadrature component of reference signal TYPE pointer
:param tgt_i: In-phase component of target signal TYPE pointer
:param tgt_q: Quadrature component of target signal TYPE pointer
:param numel_ref: total number of elements in the reference signal
:param numel_tgt: total number of elements in the target signal
:param numel_aline: number of elements in each a-line. numel_ref = numel_aline * number of a-alines
:param ksz_samp: kernel size in samples for averaging the phase
:param decimationfactor: downsampling factor - output phase is calculated at every decimationfactor'th element for each a-line
*/
template<typename Type>
void kasai_engine(Type *phi, Type *ref_i, Type *ref_q, Type *tgt_i, Type* tgt_q, const long numel_ref, const long numel_tgt, const long numel_aline, const int ksz_samp, const int decimationfactor){
    double localsum0[4] = {0, 0, 0, 0};
    const int numel_aline_out = numel_aline/decimationfactor;
    /* Loop through array */
    int idx, idxref, idxtgt, idxtgt1;
    for(idx=(0); idx<(numel_tgt+(ksz_samp)); idx++) {
        if(idx < (ksz_samp)) {
            idxref = (idx-ksz_samp/2 +  numel_ref) % numel_ref;
            idxtgt = (idx-ksz_samp/2 +  numel_tgt) % numel_tgt;
            localsum0[0] += (double)ref_q[idxref]*tgt_i[idxtgt];
            localsum0[1] += (double)ref_i[idxref]*tgt_q[idxtgt];
            localsum0[2] += (double)ref_i[idxref]*tgt_i[idxtgt];
            localsum0[3] += (double)ref_q[idxref]*tgt_q[idxtgt];
        }
        else {
            //compute the phase angle for the current buffers, assign to the output
            idxtgt = idx-ksz_samp;
            phi[(idxtgt % numel_aline)/decimationfactor + numel_aline_out*(idxtgt/numel_aline)] = (Type)atan2(localsum0[0]-localsum0[1],localsum0[2]+localsum0[3]);           
            // subtract the earliest sample from the buffers
            idxref = (idx-ksz_samp-ksz_samp/2 + numel_ref) % numel_ref;
            idxtgt = (idx-ksz_samp-ksz_samp/2 + numel_tgt) % numel_tgt;
            localsum0[0] -= (double)ref_q[idxref]*tgt_i[idxtgt];
            localsum0[1] -= (double)ref_i[idxref]*tgt_q[idxtgt];
            localsum0[2] -= (double)ref_i[idxref]*tgt_i[idxtgt];
            localsum0[3] -= (double)ref_q[idxref]*tgt_q[idxtgt];
            // add the new sample to the buffers
            idxref = (idx-ksz_samp/2 + numel_ref) % numel_ref;
            idxtgt = (idx-ksz_samp/2 + numel_tgt) % numel_tgt;
            idxtgt1 = (idx-ksz_samp/2+1 + numel_tgt) % numel_tgt;
            localsum0[0] += (double)ref_q[idxref]*tgt_i[idxtgt];
            localsum0[1] += (double)ref_i[idxref]*tgt_q[idxtgt];
            localsum0[2] += (double)ref_i[idxref]*tgt_i[idxtgt];
            localsum0[3] += (double)ref_q[idxref]*tgt_q[idxtgt];
        }
    }
}