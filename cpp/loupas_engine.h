#ifndef loupas_engine_h
#define loupas_engine_h
template<typename Type>
void loupas_engine(Type *phi, Type *ref_i, Type *ref_q, Type *tgt_i, Type *tgt_q, Type *fdemoverfs, const long numel_ref, const long numel_tgt, const long numel_aline, const long numel_fdem, const int ksz_samp, const int decimationfactor);
#endif