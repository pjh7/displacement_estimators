#ifndef normxcorr_engine_double_h
#define normxcorr_engine_double_h
#define MAX_KERNEL 1024
#define MAX_SEARCH 1024
void normxcorr_engine_double(double *shift, double*cc, double *ref_rf, double *tgt, const long numel_ref, const long numel_tgt, const long numel_aline, const long ksz_samp, const long srch_samp, const int calc_cc, const int decimationfactor);
#endif